//
//  YouRatedView.h
//  Gourmet7
//
//  Created by Mac Pro on 20/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class YouRatedView;

@protocol YouRatedViewDelegate <NSObject>

//-(void)YouRatedView:(YouRatedView *)YouRatedView ratingDidChange:(float)rating;


@end

@interface YouRatedView : UIView

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating4;
@property (assign) BOOL editable;
@property (strong) NSMutableArray *imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <YouRatedViewDelegate> delegate;


@end
