//
//  CheckinViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 11/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUser.h"
#import "SUser2.h"
//#import "detailViewController.h"

@class CheckinViewController;

@protocol CheckinViewControllerDelegate <NSObject>

- (void)PassId:(CheckinViewController *)CheckinViewController PassedId:(NSString *)ID;
- (void)PassvisitPurpose:(CheckinViewController *)CheckinViewController1 PassvisitPurpose:(NSString *)Purpose;
//-(void)passDictionary:(CheckinViewController *)CheckinViewController2 PassDict:(NSDictionary *)MyDict;
//- (void)MethodNameToCallBack:(NSString *)requestId;

@end
@interface CheckinViewController : UIViewController
{
     NSDictionary *chekDict;
}
@property (nonatomic ,weak) id <CheckinViewControllerDelegate> delegate;
@property (strong, nonatomic) NSString *OutletId;
@property (strong, nonatomic) NSString *requestId;
@property (strong, nonatomic) NSString *vpurpose;
@property (nonatomic) NSInteger RestoId;

@property (weak, nonatomic) IBOutlet UIButton *Business;
@property (weak, nonatomic) IBOutlet UIButton *Family;
@property (weak, nonatomic) IBOutlet UIButton *Friend;
@property (weak, nonatomic) IBOutlet UIButton *Romantic;
@property (weak, nonatomic) IBOutlet UIButton *Solo;
@property (nonatomic, strong) NSString *token;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) SUser *user1;
@property (strong, nonatomic) NSString *VisitPurpose;
@property (strong, nonatomic)NSDictionary *MyDictionary;

- (IBAction)BusinessBtn:(id)sender;
- (IBAction)FamilyBtn:(id)sender;
- (IBAction)FriendBtn:(id)sender;
- (IBAction)RomanticBtn:(id)sender;
- (IBAction)SoloBtn:(id)sender;

@end
