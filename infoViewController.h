//
//  infoViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 20/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RestaurantListing.h"
#import "SUser.h"
#import "detailViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

@interface infoViewController : UIViewController<MKMapViewDelegate,UICollectionViewDataSource,UICollectionViewDelegate,CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *pintitle;
@property (weak, nonatomic) IBOutlet UIView *Contentview;
@property (strong, nonatomic) IBOutlet UILabel *addresslbl;
@property (strong, nonatomic) IBOutlet UILabel *contactlbl;
@property (strong, nonatomic) IBOutlet MKMapView *mapview;
@property (strong, nonatomic) CLLocationManager *locationmanager;
@property (strong, nonatomic) SUser *user2;
@property (strong, nonatomic) NSDictionary *myDict;
@property (strong, nonatomic) NSMutableArray *originalimagesArray;
@property (strong, nonatomic) NSMutableArray *mobileimageArray;
@property (strong, nonatomic) NSMutableArray *thumbimageArray;
@property (nonatomic) NSMutableArray *mobilethumbimageArray;
@property (weak, nonatomic) IBOutlet UILabel *mondaytime;
@property (weak, nonatomic) IBOutlet UILabel *tuesdaytime;
@property (weak, nonatomic) IBOutlet UILabel *wednesdaytime;
@property (weak, nonatomic) IBOutlet UILabel *thursdaytime;
@property (weak, nonatomic) IBOutlet UILabel *fridaytime;
@property (weak, nonatomic) IBOutlet UILabel *saturdaytime;
@property (weak, nonatomic) IBOutlet UILabel *sundaytime;
@property (weak, nonatomic) IBOutlet UIImageView *acimage;
@property (weak, nonatomic) IBOutlet UIImageView *nonvegetarian;
@property (weak, nonatomic) IBOutlet UIImageView *happyhoursimg;
@property (weak, nonatomic) IBOutlet UIImageView *disabledfriendlyimg;
@property (weak, nonatomic) IBOutlet UIImageView *pvtpartyimg;
@property (weak, nonatomic) IBOutlet UIImageView *parkingimg;
@property (weak, nonatomic) IBOutlet UIImageView *dineinavailableimg;
@property (weak, nonatomic) IBOutlet UIImageView *outdoorseatingimg;
@property (weak, nonatomic) IBOutlet UIImageView *barareaimg;
@property (weak, nonatomic) IBOutlet UIImageView *livesportimg;
@property (weak, nonatomic) IBOutlet UIImageView *open24_7img;
@property (weak, nonatomic) IBOutlet UIImageView *sundaybrunchimg;
@property (weak, nonatomic) IBOutlet UIImageView *banquetimg;
@property (weak, nonatomic) IBOutlet UIImageView *partyordersimg;
@property (weak, nonatomic) IBOutlet UIImageView *themeimg;
@property (weak, nonatomic) IBOutlet UIImageView *engspeakingimg;
@property (weak, nonatomic) IBOutlet UIImageView *jainfoodimg;
@property (weak, nonatomic) IBOutlet UIImageView *buffetlunchimg;
@property (weak, nonatomic) IBOutlet UIImageView *livefoodimg;
@property (weak, nonatomic) IBOutlet UIImageView *snacksimg;
@property (weak, nonatomic) IBOutlet UIImageView *buffetdinnerimg;
@property (weak, nonatomic) IBOutlet UIImageView *takeawayimg;
@property (weak, nonatomic) IBOutlet UIImageView *selfserviceimg;
@property (weak, nonatomic) IBOutlet UIImageView *musicimg;
@property (weak, nonatomic) IBOutlet UIImageView *dresscodeimg;
@property (weak, nonatomic) IBOutlet UIImageView *corporateimg;
@property (weak, nonatomic) IBOutlet UIImageView *tablereservimg;
@property (weak, nonatomic) IBOutlet UIImageView *provideshomedelivery;
@property (weak, nonatomic) IBOutlet UIImageView *smokingimg;
@property (weak, nonatomic) IBOutlet UIImageView *wifiimg;
@property (weak, nonatomic) IBOutlet UIImageView *restroomsimg;
@property (weak, nonatomic) IBOutlet UIImageView *petfriendly;
@property (weak, nonatomic) IBOutlet UIImageView *alcoholimg;
@property (weak, nonatomic) IBOutlet UIImageView *midnightbuffetimg;
@property (weak, nonatomic) IBOutlet UIImageView *coupponsaccpted;
@property (weak, nonatomic) IBOutlet UIImageView *inmallimg;
@property (weak, nonatomic) IBOutlet UIImageView *creditcardimg;
@property (weak, nonatomic) IBOutlet UICollectionView *photocollectionview;
@property (assign, nonatomic)CLLocationCoordinate2D coordinate;
@property (copy, nonatomic)NSString *title1;
@property (nonatomic)float currentlat;
@property (nonatomic)float currentlon;

- (IBAction)OpenMapBtn:(id)sender;


@end
