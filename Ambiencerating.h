//
//  Ambiencerating.h
//  Gourmet7
//
//  Created by Mac Pro on 19/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Ambiencerating;

@protocol AmbienceratingDelegate <NSObject>

-(void)Ambiencerating:(Ambiencerating *)Ambiencerating ratingDidChange:(float)rating1;

@end
@interface Ambiencerating : UIView

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating1;
@property (assign) BOOL editable;
@property (strong) NSMutableArray *imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <AmbienceratingDelegate> delegate;

@end
