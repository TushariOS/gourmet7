//
//  DirectRtingViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 22/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatesView.h"
#import "Ambiencerating.h"
#import "Servicerating.h"
#import "ValueforMoney.h"
#import "Hygienerating.h"
#import "SUserDB.h"
#import "SUser.h"
#import "SUser2.h"
#import "AFNetworking.h"


@interface DirectRtingViewController : UIViewController <RatesViewDelegate,AmbienceratingDelegate,ServiceratingDelegate,ValueforMoneyDelegate,HygieneratingDelegate>

@property (weak, nonatomic) IBOutlet RatesView *Foodratingview;

@property (weak, nonatomic) IBOutlet Ambiencerating *Ambienceratingview;
@property (weak, nonatomic) IBOutlet Servicerating *Serviceratingview;
@property (weak, nonatomic) IBOutlet ValueforMoney *Valueformoneyview;
@property (weak, nonatomic) IBOutlet Hygienerating *HygieneRatingview;

@property (nonatomic) float rates1;
@property (nonatomic) float rates2;
@property (nonatomic) float rates3;
@property (nonatomic) float rates4;
@property (nonatomic) float rates5;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *OutletId;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) SUser2 *data2;
- (IBAction)Ratebutton:(id)sender;

@end
