//
//  MyspaceViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 03/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "MyspaceViewController.h"
#import "MySpaceTableViewCell.h"
#import "SUser2.h"

@interface MyspaceViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *Myscroll;

@end

@implementation MyspaceViewController
@synthesize Checkintable,Myscroll,MyDict,MyArray,ListArray,auser,mynotification;
- (void)viewDidLoad {
    [super viewDidLoad];
  //  [self reloadmyspace:mynotification];
        }



- (void)reloadmyspace:(NSNotification *)anote
{
    
  //  Checkintable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
//[self.Checkintable addObserver:self forKeyPath:@"contentSize" options:0 context:NULL];
    MyDict = [anote userInfo];
    
    MyArray = [MyDict objectForKey:@"checkin"];
    
    if (!MyArray || !MyArray.count)
    {
        self.Checkintable.hidden = YES;
    }
    [self FetchData];
    [[self Checkintable]setDataSource:self];
    [[self Checkintable]setDelegate:self];
    [[self Checkintable ]reloadData];
    
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.Myscroll layoutIfNeeded];
    self.Myscroll.contentSize = self.Contentview.bounds.size;
    self.Myscroll.contentSize = CGSizeMake(320.0f, 700.0f);
    //Checkintable.frame = CGRectMake(Checkintable.frame.origin.x, Checkintable.frame.origin.y, Checkintable.frame.size.width, Checkintable.contentSize.height);
}

-(void)FetchData
{
  //  NSString*    mystring = @"";
   // NSString *a = nil;
    ListArray = [[NSMutableArray alloc]init];
    for (id jsonData in MyArray)
    {
        SUser2 *data = [SUser2 new];
        
        NSString* Checkindate = [[jsonData valueForKey:@"checkInDate"]stringValue ];
       // NSString* Checkintime = [[jsonData valueForKey:@"checkInTime"] stringValue];
        data.purpose = [jsonData valueForKey:@"visitPurpose"];
        data.SavedAmt = [[jsonData valueForKey:@"savedAmount"]stringValue];
    
        NSString* timeStampString =Checkindate;
        NSTimeInterval interval=[timeStampString doubleValue];
        NSTimeInterval times = interval/1000;
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:times];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"dd MMM,yyyy"];
        NSString *_date=[_formatter stringFromDate:date];
        
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc]init];
        [formatter1 setDateFormat:@"hh:mm a"];
        
        NSString *date1 = [formatter1 stringFromDate:date];
        data.finaldate = _date;
        data.finaltime = date1;
               if (!ListArray) ListArray = [[NSMutableArray alloc] init];
                ListArray.removeLastObject;
               [ListArray addObject:data];
    }
}
-(void)showdata
{
    
}
-(void)viewDidAppear:(BOOL)animated
{
   // Checkintable.frame = CGRectMake(Checkintable.frame.origin.x, Checkintable.frame.origin.y, Checkintable.frame.size.width, Checkintable.contentSize.height);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [ListArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;

{
    static NSString *cellidentifier = @"cell";
    
    MySpaceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell == nil)
    {
        cell = [[MySpaceTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
    }
    auser = [ListArray objectAtIndex:indexPath.row];
    cell.Datelabel.text = auser.finaldate;
    cell.timelabel.text = auser.finaltime;
    cell.purposelabel.text = auser.purpose;
    cell.savedlabel.text = auser.SavedAmt;
    return cell;
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadmyspace:) name:@"myspace" object:nil];

}

@end
