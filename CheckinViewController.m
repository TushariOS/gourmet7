//
//  CheckinViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 11/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "CheckinViewController.h"
#import "detailViewController.h"
#import "SUser.h"
#import "SUserDB.h"
#import "RestaurantListing.h"
#import "SUser2.h"
#import "AFNetworking.h"

@interface CheckinViewController ()


@end

@implementation CheckinViewController
@synthesize token,OutletId,userId,RestoId,VisitPurpose,requestId,delegate,vpurpose,MyDictionary;

//-(void)reloadCheckin:(NSNotification *)anote1
//{
//    chekDict = [anote1 userInfo];
//    NSLog(@"checkdictionary is %@",chekDict);
//}

- (void)viewDidLoad
{
    
    token =[[SUserDB singleton]fetchTokenNumber];
    userId =[[SUserDB singleton]fetchuserId];
   // detailViewController *nVC = [[detailViewController alloc]init];
    
   
        //[self.delegate passDictionary:self PassDict:self.MyDictionary];
    //[self.delegate MethodNameToCallBack:requestId];
    NSLog(@"%@",chekDict);
   }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void)Checkinurl
{
    NSString *post =[[NSString alloc] initWithFormat:@"outletId=%@&userId=%@&visitPurpose=%@&individualVisit=0&lastVisitedOutletId=0&token=%@&url=/DeviceServerPush/checkIn",OutletId, userId,VisitPurpose, token];
    NSLog(@"PostData: %@",post);
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    NSString *URLString = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth"];
    NSDictionary *params = @{@"outletId":OutletId,
                             @"userId": userId,
                             @"visitPurpose":VisitPurpose,
                             @"individualVisit":@"0",
                             @"lastVisitedOutletId":@"0",
                             @"token":token,
                             @"url":@"/DeviceServerPush/checkIn"};
    
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self checkinresponse:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];

}

-(void)checkinresponse:(NSDictionary *)jSonData
{
    SUser2 *data1 = [SUser2 new];
    NSDictionary *outletDict = [jSonData objectForKey:@"outlet"];
    
    data1.RequestId = [jSonData valueForKey:@"requestId"];
    data1.RestoId = [outletDict valueForKey:@"outletId"];
    data1.Restoname = [outletDict valueForKey:@"name"];
    data1.visitpurpose = [jSonData valueForKey:@"visitPurpose"];
    [[SUserDB singleton]saverestodetails:data1];
    
    
    
    
    requestId = [jSonData valueForKey: @"requestId"];
    vpurpose = [jSonData valueForKey:@"visitPurpose"];
    
    [self.delegate PassId:self PassedId:self.requestId];
    [self.delegate PassvisitPurpose:self PassvisitPurpose:self.vpurpose];
}

- (IBAction)BusinessBtn:(id)sender
{
    VisitPurpose = @"1";
     [self Checkinurl];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)FamilyBtn:(id)sender
{
    VisitPurpose = @"2";
    [self Checkinurl];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)FriendBtn:(id)sender
{
    VisitPurpose = @"3";
    [self Checkinurl];
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)RomanticBtn:(id)sender
{
    VisitPurpose =@"4";
    [self Checkinurl];
    
    [self dismissViewControllerAnimated:YES completion:Nil];
}

- (IBAction)SoloBtn:(id)sender
{
    VisitPurpose = @"5";
    [self Checkinurl];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
