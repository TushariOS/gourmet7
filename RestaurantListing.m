//
//  RestaurantListing.m
//  Gourmet7
//
#import "RestaurantListing.h"
#import "ViewController.h"
#import "SUser.h"
#import "SUserDB.h"
#import "customcell.h"
#import "detailViewController.h"
#import "UIImageView+WebCache.h"
#import "RateView.h"
#import <CoreLocation/CoreLocation.h>
#import "RegisterViewController.h"
#import "AFNetworking.h"
#import "FilterViewController.h"
#define TimeStamp [NSString stringWithFormat:@"%lu",[[NSDate date] timeIntervalSince1970 ]];
@interface RestaurantListing ()<CLLocationManagerDelegate>
@end
@implementation RestaurantListing
{
    UIView *loadingView;
    CLLocationManager *locationmanager;
    NSMutableArray *arrayofresto;
    NSMutableArray *arrayofresto1;
}
@synthesize test,start,token,openbtn,complibtn,followbtn,discbtn,currenttime,opentime,serviceResponse1,auser,theLocation,userId,follow1,Active1,button,DistInKilometers,currentlat,currentlon,MyFilterArray,shouldreload,Filter1,Filter2,Filter3,Restoname,Cuisine,Area,MyArray1,RestaurantName,CuisineName,AreaName,sidebarbutton,followresto,FilterArray,locationManager;

- (void)viewDidLoad{
    
    // Objective-C
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest ;
    [self.locationManager requestWhenInUseAuthorization];
    [self.locationManager startUpdatingLocation];

    UIImage *logoimage = [UIImage imageNamed:@"logo_small.png"];
    
    self.navigationItem.titleView = [[UIImageView alloc]initWithImage:logoimage];
    
    self.revealViewController.delegate = self;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarbutton setTarget: self.revealViewController];
        [self.sidebarbutton setAction: @selector( mysidebarAnimated:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
[self loadingView];
    
    onfirstvisit = YES;
    moreitemsavailable = YES;
    
   Filter1  = [[NSMutableArray alloc]initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:@"Sun-Day-Normal.png",@"images",@"open",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc] initWithObjectsAndKeys:@"Offer-Normal.png",@"images",@"Offers",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Promocodes-Normal.png",@"images",@"Promocodes",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Discount-Normal.png",@"images",@"Discount",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Complimentary-Normal.png",@"images",@"Complimentary",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Followers-Normal02.png",@"images",@"Following",@"titles",@"false",@"selected", nil],nil];
    Filter2 = [[NSMutableArray alloc]initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:@"Self-Service-Normal.png",@"images",@"Call for service",@"titles",@"false",@"selected", nil],
                    [[NSDictionary alloc]initWithObjectsAndKeys:@"Table-reserved-Normal.png",@"images",@"Table Booking",@"titles",@"false",@"selected", nil],
                    [[NSDictionary alloc]initWithObjectsAndKeys:@"Happy-H-Normal.png",@"images",@"Happy Hours",@"titles",@"false",@"selected", nil],nil];
    
    Filter3 = [[NSMutableArray alloc]initWithObjects:[[NSDictionary alloc]initWithObjectsAndKeys:@"AC-Normal.png",@"images",@"AC",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"CCard-Normal.png",@"images",@"CreditCard",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Wifi-Normal.png",@"images",@"Wi-Fi",@"titles",@"false",@"selected", nil],
                     [[ NSDictionary alloc]initWithObjectsAndKeys:@"non_veg-Normal.png",@"images",@"Non-Vegetarian",@"titles",@"false",@"selected",nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Parking-Normal.png",@"images",@"Parking",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Pet-Friendly-Normal.png",@"images",@"Pet Friendly",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Toilet-Normal.png",@"images",@"Toilet",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Servers-Alcohol-Normal.png",@"images",@"Serves Alcohol",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Smoking-Normal.png",@"images",@"Smoking",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Waiters-Normal.png",@"images",@"Eng. Speaking Waiters",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Privet-party-Normal.png",@"images",@"Pvt. Party Area",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Disable-Fraindly-Normal.png",@"images",@"Disabled Friendly",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Live-food-counter-Normal.png",@"images",@"Live Food counter",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Take-away-Normal.png",@"images",@"Take Away",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Snacks-Normal.png",@"images",@"Snacks",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Dine-in-Normal.png",@"images",@"Dine in",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"outdoor-seating-Normal.png",@"images",@"Outdoor Seating",@"titles",@"false",@"selected",nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Table-reserved-Normal.png",@"images",@"Table Reservation",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Bar-Area-Normal.png",@"images",@"Bar Area",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Plays-Music-Normal.png",@"images",@"Play Music",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Home-Delivery-Normal.png",@"images",@"Home Delivery",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Corporate-Normal.png",@"images",@"Corporate",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Live-Sports-Normal.png",@"images",@"Live Sports",@"titles",@"false",@"selected",nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Banquet-Normal.png",@"images",@"Banquet",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Dress-Code-Normal.png",@"images",@"Dress Code",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"24x7-Normal.png",@"images",@"Open 24x7",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Sunday-Bunch-Normal.png",@"images",@"Sunday Brunch",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Self-Service-Normal.png",@"images",@"Self Service",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Jain-Food-Normal.png",@"images",@"Jain Food",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Buffet-Lunch-Normal.png",@"images",@"Buffet Lunch",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Buffet-Dinner-Normal.png",@"images",@"Buffet Dinner",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Coupan-accepted-Normal.png",@"images",@"Coupons Accepted",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Moon-Night-Normal.png",@"images",@"Midnight Buffet",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Party-Orders-Normal.png",@"images",@"Party Orders",@"titles",@"false",@"selected",nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Theme-Normal.png",@"images",@"Theme",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"In-Mall-Normal.png",@"images",@"In-Mall",@"titles",@"false",@"selected", nil],nil];
                       MyFilterArray = [NSMutableArray arrayWithObjects:Filter1,Filter2,Filter3, nil];
    
    
    if (FilterArray != nil)
    {
        MyFilterArray = FilterArray;

    }
    [self mymethod];
    
}

-(void)loadingView
{
    loadingView = [[UIView alloc]initWithFrame:CGRectMake(100, 400, 80, 80)];
    CGRect frame = loadingView.frame;
    frame.origin.x = self.view.frame.size.width / 2 - frame.size.width / 2;
    frame.origin.y = self.view.frame.size.height / 2 - frame.size.height / 2;
    loadingView.frame = frame;
    loadingView.backgroundColor = [UIColor clearColor];
    
    loadingView.layer.cornerRadius = 5;
    
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center = CGPointMake(loadingView.frame.size.width / 2.0, 35);
    [activityView startAnimating];
    activityView.tag = 100;
    [loadingView addSubview:activityView];
    [self.view addSubview:loadingView];
}

-(void)mymethod
{
    NSLog(@"filter array is %@",MyFilterArray);
    [[self restotable]setDelegate:self];
    [[self restotable]setDataSource:self];
    token =[[SUserDB singleton]fetchTokenNumber];
    userId =[[SUserDB singleton]fetchuserId];
    [self callservicewithsession];
    
    locationmanager = [[CLLocationManager alloc]init];
    locationmanager.distanceFilter = kCLDistanceFilterNone;
    locationmanager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationmanager startUpdatingLocation];
    locationmanager.delegate = self;
}
- (NSString *)deviceLocation
{
    NSString *theLocation = [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
    return theLocation;
}

-(void)callservicewithsession
{
    Filter1 = [MyFilterArray objectAtIndex:0];
    Filter2 = [MyFilterArray objectAtIndex:1];
    Filter3 = [MyFilterArray objectAtIndex:2];
    NSString *openstr = [[Filter1 objectAtIndex:0] valueForKey:@"selected"];
    NSString *Offer = [[Filter1 objectAtIndex:1]valueForKey:@"selected"];
    NSString *Promocode = [[Filter1 objectAtIndex:2]valueForKey:@"selected"];
    NSString *Discount = [[Filter1 objectAtIndex:3]valueForKey:@"selected"];
    NSString *Complimetry = [[Filter1 objectAtIndex:4]valueForKey:@"selected"];
    NSString *Following = [[Filter1 objectAtIndex:5]valueForKey:@"selected"];
    
    NSString *Callforservice = [[Filter2 objectAtIndex:0]valueForKey:@"selected"];
    NSString *Tablebooking = [[Filter2 objectAtIndex:1]valueForKey:@"selected"];
    NSString *Happyhrs = [[Filter2 objectAtIndex:2]valueForKey:@"selected"];
    NSString *Ac = [[Filter3 objectAtIndex:0]valueForKey:@"selected"];
    NSString *CreditCard = [[Filter3 objectAtIndex:1]valueForKey:@"selected"];

    NSString *Wifi = [[Filter3 objectAtIndex:2]valueForKey:@"selected"];

    NSString *Nonveg = [[Filter3 objectAtIndex:3]valueForKey:@"selected"];

    NSString *Parking = [[Filter3 objectAtIndex:4]valueForKey:@"selected"];

    NSString *Petfriendly = [[Filter3 objectAtIndex:5]valueForKey:@"selected"];

    NSString *Toilet = [[Filter3 objectAtIndex:6]valueForKey:@"selected"];

    NSString *ServesAlcoho = [[Filter3 objectAtIndex:7]valueForKey:@"selected"];

    NSString *Smoking = [[Filter3 objectAtIndex:8]valueForKey:@"selected"];
    NSString *EngSpeakingwtrs = [[Filter3 objectAtIndex:9]valueForKey:@"selected"];

    NSString *Pvtpartyarea = [[Filter3 objectAtIndex:10]valueForKey:@"selected"];
    NSString *Disabledfriedly = [[Filter3 objectAtIndex:11]valueForKey:@"selected"];
    NSString *Livefoodcounter = [[Filter3 objectAtIndex:12]valueForKey:@"selected"];
    NSString *takeaway = [[Filter3 objectAtIndex:13]valueForKey:@"selected"];
    NSString *Snacks = [[Filter3 objectAtIndex:14]valueForKey:@"selected"];
    NSString *DineIn = [[Filter3 objectAtIndex:15]valueForKey:@"selected"];
    NSString *OutdoorSeating = [[Filter3 objectAtIndex:16]valueForKey:@"selected"];
    NSString *Tablereservation = [[Filter3 objectAtIndex:17]valueForKey:@"selected"];
    NSString *BarArea = [[Filter3 objectAtIndex:18]valueForKey:@"selected"];
    NSString *Playmusic = [[Filter3 objectAtIndex:19]valueForKey:@"selected"];
    NSString *HomeDelivery = [[Filter3 objectAtIndex:20]valueForKey:@"selected"];
    NSString *Corporate = [[Filter3 objectAtIndex:21]valueForKey:@"selected"];
    NSString *LiveSports = [[Filter3 objectAtIndex:22]valueForKey:@"selected"];
    NSString *Banquet = [[Filter3 objectAtIndex:23]valueForKey:@"selected"];
    NSString *Dresscode = [[Filter3 objectAtIndex:24]valueForKey:@"selected"];
    NSString *Open24x7 = [[Filter3 objectAtIndex:25]valueForKey:@"selected"];
    NSString *SundayBrunch = [[Filter3 objectAtIndex:26]valueForKey:@"selected"];
    NSString *Selfservice = [[Filter3 objectAtIndex:27]valueForKey:@"selected"];
    NSString *JainFood = [[Filter3 objectAtIndex:28]valueForKey:@"selected"];
    NSString *BuffetLunch = [[Filter3 objectAtIndex:29]valueForKey:@"selected"];
    NSString *BuffetDinner = [[Filter3 objectAtIndex:30]valueForKey:@"selected"];
    NSString *CouponsAccepted = [[Filter3 objectAtIndex:31]valueForKey:@"selected"];
    NSString *MidnightBuffet = [[Filter3 objectAtIndex:32]valueForKey:@"selected"];
    NSString *PartyOrders = [[Filter3 objectAtIndex:33]valueForKey:@"selected"];
    NSString *Theme = [[Filter3 objectAtIndex:34]valueForKey:@"selected"];
    NSString *Inmall = [[Filter3 objectAtIndex:35]valueForKey:@"selected"];
    

    if (FilterArray==nil)
    {
        RestaurantName = @"";
        CuisineName = @"";
        AreaName = @"";
        
    }
    else
    {
        NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@" "];
        RestaurantName = [[Restoname componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @"%20"];
        NSLog(@"%@", RestaurantName);
        CuisineName = Cuisine;
        AreaName = Area;
    }
   if ([openstr isEqualToString:@"true"])
    {
        opentime= TimeStamp;
    }
    else
    {
        opentime = @"";
        
    }
    NSString* Listurl = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=%%2FRestaurant%%2FAdvancedSearch&cityId=%@&name=%@&area=&cuisines=&smokingAllowed=%@&servesAlcohol=%@&acceptsCreditCard=%@&homeDelivery=%@&ac=%@&wifi=%@&music=%@&happyHours=%@&takeAway=%@&snacks=%@&toilet=%@&dinein=%@&parking=%@&nonvegeterian=%@&outdoorSeating=%@&tableReservation=%@&barArea=%@&corporateEvents=%@&liveSportsBroadcast=%@&dressCode=%@&open24_7=%@&privatePartyArea=%@&sundayBrunch=%@&selfServices=%@&petFriendly=%@&jainFood=%@&buffetLunch=%@&buffetDinner=%@&banquet=%@&midnightBuffet=%@&partyOrders=%@&disablFacility=%@&theme=%@&inMall=%@&englishSpeakingWaiters=%@&couponsAccepted=%@&liveFoodCounter=%@&offer=%@&promotion=%@&discount=%@&complementary=%@&following=%@&callForServices=%@&tableBooking=%@&start=%ld&end=10&opentime=%@&token=%@",[[SUserDB singleton]fetchcityId],RestaurantName, Smoking,ServesAlcoho,CreditCard,HomeDelivery,Ac, Wifi,Playmusic,Happyhrs,takeaway,Snacks,Toilet,DineIn,Parking,Nonveg,OutdoorSeating,Tablereservation,BarArea,Corporate,LiveSports,Dresscode,Open24x7,Pvtpartyarea,SundayBrunch,Selfservice,Petfriendly,JainFood,BuffetLunch,BuffetDinner,Banquet,MidnightBuffet,PartyOrders,Disabledfriedly,Theme,Inmall,EngSpeakingwtrs,CouponsAccepted,Livefoodcounter,Offer,Promocode,Discount,Complimetry,Following,Callforservice, Tablebooking,start,opentime,token];
    
    
   

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];

    [manager GET:Listurl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self Processwithdata:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
    
}
#pragma parsing data

-(void)Processwithdata:(NSArray *)data1

{
    if (data1.count ==10)
    {
        moreitemsavailable = YES;
    }
    else
    {
        moreitemsavailable = NO;
    }
    if (onfirstvisit == YES &&data1.count == 0)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                            message:@"No such Restaurant found please modify your search"
                                                           delegate:self
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil, nil];
        [alertView show];
    }
    
       for (id jsonData in data1)
       {
               SUser*  data = [SUser new];
                 data.outletId=[[jsonData objectForKey:@"outletId"]stringValue];
                 data.name = [jsonData objectForKey: @"name"];
                 data.todaysDiscount = [[jsonData objectForKey:@"todaysDiscount"]stringValue];
                 data.complementary = [jsonData objectForKey:@"complementary"];
                 data.avgRating = [[jsonData objectForKey:@"avgRating"]stringValue];
                 data.address = [jsonData objectForKey:@"address"];
                 data.telephone1 = [jsonData objectForKey:@"telephone1"];
                 data.areaName = [jsonData objectForKey:@"areaName"];
                 data.logoimagepath = [jsonData objectForKey:@"logopath"];
                 data.specialty = [jsonData objectForKey:@"specialty"];
                 data.opened = [[jsonData valueForKey:@"opened"]boolValue];
                 data.compliment = [[jsonData valueForKey:@"compliment"]boolValue];
                 data.latitude = [[jsonData valueForKey:@"latitude"]doubleValue];
                 data.longitude = [[jsonData valueForKey:@"longitude"]doubleValue];
                 NSLog(@"%f",data.latitude);
                 NSLog(@"%f",data.longitude);
                 data.Active =[[jsonData objectForKey:@"followed"]boolValue];
                 data.phonenumber= [jsonData valueForKey:@"telephone1"];
           if (!arrayofresto) arrayofresto =[[NSMutableArray alloc] init];
          [arrayofresto addObject:data];
        }
    NSLog(@"response is :%@",data1);
    
     start = [arrayofresto count];
    if (start > 1)
    {
        [[ self restotable]reloadData];
        [loadingView setHidden:NO];
    }
}


#pragma mark -tableviewdelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrayofresto count];
   
}
#pragma mark -cell for row

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  static NSString *cellidentifier = @"cell";
    
    customcell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell == nil)
    {
        cell = [[customcell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
    }
    auser = [arrayofresto objectAtIndex:indexPath.row];
    cell.restonamelbl.text= auser.name;
    cell.Arealbl.text = auser.areaName;
     NSLog(@"%@",auser.areaName);
    cell.discountlbl.text = [NSString stringWithFormat:@"%@%%",auser.todaysDiscount];
    cell.restonamelbl.textColor = [UIColor blueColor];
    cell.rateview.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    cell.rateview.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    cell.rateview.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    cell.rateview.rating = [auser.avgRating floatValue];
    cell.rateview.editable = NO;
    cell.rateview.maxRating = 5;
    cell.rateview.delegate = cell;
    cell.Speciality.text = auser.specialty;
    Active1 = auser.Active;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
    [view setBackgroundColor:[UIColor greenColor]];
    cell.editingAccessoryView = view;
   if (Active1 == YES )
    {
        [cell.followbtn setBackgroundImage:[UIImage imageNamed:@"Followers1.png"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.followbtn setBackgroundImage:[UIImage imageNamed:@"Followers-unfilled.png"] forState:UIControlStateNormal];
    }
    cell.followbtn.tag = indexPath.row;
    [cell.followbtn addTarget:self
action:@selector(aMethod:RestoId:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.callbutton.tag = indexPath.row;
    [cell.callbutton addTarget:self action:@selector(makeAcall:Phonenumber:) forControlEvents:UIControlEventTouchUpInside];
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:currentlat longitude:currentlon];
     NSLog(@"distance A is : %@",locA);
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:auser.latitude longitude:auser.longitude];
     NSLog(@"distance B is : %@",locB);
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    
    DistInKilometers = distance /1000.0;
    cell.Distancelbl.text = [NSString stringWithFormat:@"%.2fkm",DistInKilometers];
     NSLog(@"distance is : %f",DistInKilometers);
    
    BOOL door = auser.opened;
    if (door)
    {
        cell.doorimg.image = [UIImage imageNamed:@"Door-Open.png"];
    }
    else
    {
        cell.doorimg.image = [UIImage imageNamed:@"Door-Close.png"];
    }

    BOOL compliment = auser.compliment;
    if (compliment)
    {
        cell.compliimage.image = [UIImage imageNamed:@"Complimentary-Clicks.png"];
    }
    else
    {
        cell.compliimage.image = [UIImage imageNamed:@""];
    }
    
    [loadingView setHidden:YES];
    if (indexPath.row ==  arrayofresto.count-1)
    {
        if (moreitemsavailable)
        {
            [loadingView setHidden:NO];
            [self callservicewithsession];
        }
        else
        {
            
        }
    }
return cell;
}

-(void)makeAcall:(UIButton*)sender Phonenumber:(NSString*)phonenum
{
    followresto = [SUser new];
    followresto = [arrayofresto objectAtIndex:sender.tag];
    phonenum = followresto.phonenumber;
    NSString *tel = [NSString stringWithFormat:@"tel:%@",phonenum];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:tel]];
}
-(void)aMethod:(UIButton*)sender RestoId:(NSString*)OutletId1

{
    followresto = [SUser new];
    followresto = [arrayofresto objectAtIndex:sender.tag];
    
    OutletId1 = followresto.outletId;
    Active1 = followresto.Active;
    if( Active1 == YES)
            {
            [sender setBackgroundImage:[UIImage imageNamed:@"Followers-unfilled.png"] forState:UIControlStateNormal];
            }
            else
            {
                [sender setBackgroundImage:[UIImage imageNamed:@"Followers1.png"] forState:UIControlStateNormal];
                }
    
    [self followrestourl:sender RestoId:OutletId1];
    NSLog(@"I Clicked a button %ld",(long)sender.tag);
}


//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//}
-(void)followrestourl:(UIButton *)sender RestoId:(NSString*)outletId

{
    NSString *follow2;
    if (Active1 == YES)
    {
        follow2 = @"false";
    }
    else
    {
        follow2 = @"true";
    }
   NSString *followurl = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=/Restaurant/FollowOutlet/%@/%@/%@&token=%@",userId,outletId,follow2, token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    [manager POST:followurl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
        if (responseObject ==nil)
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"Followers-Normal02.png"] forState:UIControlStateNormal];
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@""
                                                                message:@"unable to follow restaurant"
                                                               delegate:nil
                                                      cancelButtonTitle:nil
                                                      otherButtonTitles:@"cancel", nil];
            [alertView show];
        }
        else
        {
          //  if([[[responseObject objectForKey:@"active"]stringValue]  isEqual: @"0"]){
                start=0;
                
            
            
                    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
                    NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:0];
                    
                    [newDict addEntriesFromDictionary:oldDict];
                    [newDict setObject:@"false"forKey:@"selected"];
                    [Filter1 replaceObjectAtIndex:0 withObject:newDict];
         
                    [arrayofresto removeAllObjects];
                    [loadingView setHidden:NO];
                    [self callservicewithsession];
                    [self.restotable reloadData];
                
//            }else{
//                
//            
//            }
        }
        [self Parseforfollow:responseObject];
    }
          failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];

}
-(void)Parseforfollow:(NSMutableArray *)response
{
    auser.Active = [[response valueForKey:@"follow"] boolValue];
    if (auser.Active == NO)
    {
        [[self restotable]reloadData];
    }
   
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
   
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation* loc = [locations lastObject];
    float latitude = loc.coordinate.latitude;
    float longitude = loc.coordinate.longitude;
    NSLog(@"%.8f",latitude);
    NSLog(@"%.8f",longitude);
    
    currentlat = latitude;
    currentlon = longitude;
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
  
    float latitude = newLocation.coordinate.latitude;
    float longitude = newLocation.coordinate.longitude;
    NSLog(@"%.8f",latitude);
    NSLog(@"%.8f",longitude);
    
    currentlat = latitude;
    currentlon = longitude;


}

-(void) viewDidAppear:(BOOL)animated{

   // [loadingView setHidden:YES];
//    [self callservicewithsession];
//    [self.restotable reloadData];
       // [self viewDidLoad];


}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showdetailview"])
    {
        NSIndexPath *indexpath = [self.restotable indexPathForSelectedRow];
      
        detailViewController *destVCobj = segue.destinationViewController;
        destVCobj.user = [arrayofresto objectAtIndex:indexpath.row];
       
        
       }
    
    if ([segue.identifier isEqualToString:@"showfilter"])
    {
        FilterViewController *filter =  (FilterViewController *) segue.destinationViewController;
        filter.delegate = self;
    }
}
-(IBAction)phoneCall:(id)sender
{
    NSLog(@"phonecall");
    
}

-(IBAction)offers:(id)sender
{
    
}

-(IBAction)promoCodes:(id)sender
{
    
}

-(IBAction)showOnMap:(id)sender
{
    
}
- (IBAction)OpenNow:(id)sender
{
    start=0;
    
    NSString *openstr = [[Filter1 objectAtIndex:0]valueForKey:@"selected"];
    if([openstr isEqualToString:@"false"])
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:0];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"true"forKey:@"selected"];
        [Filter1 replaceObjectAtIndex:0 withObject:newDict];
    }
    else
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:0];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"false" forKey:@"selected"];
        [Filter1 replaceObjectAtIndex:0 withObject:newDict];
        
    }
    if (open == true)
    {
        [openbtn setBackgroundImage:[UIImage imageNamed:@"Sun-Day-Normal.png"] forState:UIControlStateNormal];
        open = false;
    }
    else
    {
        [openbtn setBackgroundImage:[UIImage imageNamed:@"Sun-Day-Clickl.png"] forState:UIControlStateNormal];
        open = true;
    }
    [arrayofresto removeAllObjects];
    [loadingView setHidden:NO];
    [self callservicewithsession];
    [self.restotable reloadData];

}
- (IBAction)Discount:(id)sender
{
    start=0;
    NSString *Discountstr = [[Filter1 objectAtIndex:3]valueForKey:@"selected"];
    if( [Discountstr isEqualToString:@"false"])
    {
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:3];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"true" forKey:@"selected"];
        [Filter1 replaceObjectAtIndex:3 withObject:newDict];
    }
    else
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:3];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"false" forKey:@"selected"];
        [Filter1 replaceObjectAtIndex:3 withObject:newDict];
            }
    
    if (disc == true)
    {
        [discbtn setBackgroundImage:[UIImage imageNamed:@"Discount-Normal.png"] forState:UIControlStateNormal];
        disc = false;
    }
    else
    {
        [discbtn setBackgroundImage:[UIImage imageNamed:@"Discount-Click.png"] forState:UIControlStateNormal];
        disc = true;
    }
    [arrayofresto removeAllObjects];
    [loadingView setHidden:NO];
    [self callservicewithsession];
    [self.restotable reloadData];
}

- (IBAction)Complementary:(id)sender
{
    start=0;
    
    NSString *complistr = [[Filter1 objectAtIndex:4]valueForKey:@"selected"];
           if( [complistr isEqualToString:@"false"])
        {
            
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:5];
            
            [newDict addEntriesFromDictionary:oldDict];
            [newDict setObject:@"true" forKey:@"selected"];
            [Filter1 replaceObjectAtIndex:4 withObject:newDict];
                        }
        else
        {
            NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
            NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:5];
            
            [newDict addEntriesFromDictionary:oldDict];
            [newDict setObject:@"false"forKey:@"selected"];
            [Filter1 replaceObjectAtIndex:4 withObject:newDict];
            
        }
    
    if (compli == true)
    {
        [complibtn setBackgroundImage:[UIImage imageNamed:@"Complimentary-Normal.png"] forState:UIControlStateNormal];
        compli = false;
    }
    else
    {
        [complibtn setBackgroundImage:[UIImage imageNamed:@"Complimentary-Clicks.png"] forState:UIControlStateNormal];
        compli = true;
    }
    [arrayofresto removeAllObjects];
    [loadingView setHidden:NO];
    [self callservicewithsession];
    [self.restotable reloadData];
}
- (IBAction)Following:(id)sender
{
    start=0;
    
    NSString *followstr = [[Filter1 objectAtIndex:5] valueForKey:@"selected"];
    
    
   
    if( [followstr isEqualToString:@"false"])
        
    {
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:5];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"true" forKey:@"selected"];
        [Filter1 replaceObjectAtIndex:5 withObject:newDict];
        
        
}
    else
    {
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[Filter1 objectAtIndex:5];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"false" forKey:@"selected"];
        [Filter1 replaceObjectAtIndex:5 withObject:newDict];
        
        }
    if (follow ==true)
    {
        [followbtn setBackgroundImage:[UIImage imageNamed:@"Followers-Normal02.png"] forState:UIControlStateNormal];
        follow = false;
        
    }
    else
    {
        [followbtn setBackgroundImage:[UIImage imageNamed:@"Followers-Click.png"] forState:UIControlStateNormal];
        follow = true;
    }
    [arrayofresto removeAllObjects];
    [loadingView setHidden:NO];
    [self callservicewithsession];
    [self.restotable reloadData];
}

-(void)PassvisitPurpose:(FilterViewController *)FilterViewController PassvisitPurpose:(NSMutableArray *)MyArray
{
    NSLog(@"myFilter Array is %@",MyArray);
    MyFilterArray = MyArray;
    shouldreload = YES;
    moreitemsavailable = YES;
    onfirstvisit = YES;
    start = 0;
}
-(void)PassId:(FilterViewController *)FilterViewController PassedId:(NSMutableArray *)ID
{
    Restoname = [ID objectAtIndex:0];
    Cuisine = [ID objectAtIndex:1];
    Area = [ID objectAtIndex:2];
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"My filter Array %@",MyFilterArray);
    if (shouldreload)
    {
     [arrayofresto removeAllObjects];
         [self mymethod];
        shouldreload = NO;
        [[self restotable]reloadData];
    }
    
   
}

@end
