//
//  RatesView.h
//  Gourmet7
//
//  Created by Mac Pro on 18/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RatesView;

@protocol RatesViewDelegate <NSObject>

-(void)ratesView:(RatesView *)RatesView ratingDidChange:(float)rating;
@end
@interface RatesView : UIView

@property (strong, nonatomic) UIImage *notSelectedImage;
@property (strong, nonatomic) UIImage *halfSelectedImage;
@property (strong, nonatomic) UIImage *fullSelectedImage;
@property (assign, nonatomic) float rating;
@property (assign) BOOL editable;
@property (strong) NSMutableArray *imageViews;
@property (assign, nonatomic) int maxRating;
@property (assign) int midMargin;
@property (assign) int leftMargin;
@property (assign) CGSize minImageSize;
@property (assign) id <RatesViewDelegate> delegate;


@end
