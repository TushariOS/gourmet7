//
//  infoViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 20/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "infoViewController.h"
#import "SUser.h"
#import "RestaurantListing.h"
#import "UIImageView+WebCache.h"
#import "MKAnnotationView+WebCache.h"
#import "CollectionViewCell.h"
#import <CoreLocation/CoreLocation.h>
#import "Annotation.h"
#import "ShowPicViewController.h"

@interface infoViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@end

@implementation infoViewController
{
    CLLocationManager *locationmanager;
    
}
@synthesize addresslbl,contactlbl,mapview,user2,myDict,mondaytime,tuesdaytime,wednesdaytime,thursdaytime,fridaytime,saturdaytime,sundaytime,acimage,creditcardimg,alcoholimg,happyhoursimg,nonvegetarian,disabledfriendlyimg,petfriendly,restroomsimg,wifiimg,dineinavailableimg,pvtpartyimg,parkingimg,partyordersimg,outdoorseatingimg,smokingimg,provideshomedelivery,tablereservimg,barareaimg,corporateimg,dresscodeimg,musicimg,livefoodimg,livesportimg,open24_7img,selfserviceimg,sundaybrunchimg,jainfoodimg,takeawayimg,buffetdinnerimg,buffetlunchimg,banquetimg,midnightbuffetimg,snacksimg,themeimg,inmallimg,coupponsaccpted,engspeakingimg,mobileimageArray,mobilethumbimageArray,thumbimageArray,originalimagesArray,coordinate,title1,currentlon,currentlat,pintitle,Contentview;


- (void)reloadInfo:(NSNotification *)anote
{
    myDict = [anote userInfo];
    
    [self getdata];
    [self mondaytiming];
    [self tuesdaytiming];
    [self wednesdaytiming];
    [self thursdaytiming];
    [self fridaytiming];
    [self saturdaytiming];
    [self sundaytiming];
    [self outletimage1];
    [self booleanmethod];
    
    [[self photocollectionview]setDataSource:self];
    [[self photocollectionview]setDelegate:self];
    [self.photocollectionview reloadData];
    self.photocollectionview.scrollEnabled = YES;
    
    locationmanager = [[CLLocationManager alloc]init];
    locationmanager.distanceFilter = kCLDistanceFilterNone;
    locationmanager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationmanager startUpdatingLocation];
    locationmanager.delegate = self;
    
    
    
    
    CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(user2.latitude, user2.longitude);
    
    MKCoordinateSpan span = MKCoordinateSpanMake(0.1, 0.1);
    MKCoordinateRegion region = {coord, span};
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:coord];
    annotation.title = user2.address;
    pintitle.text = annotation.title;
    
    
    [self.mapview setRegion:region];
    [self.mapview addAnnotation:annotation];
    
    

    
//    MKCoordinateRegion myregion;
//    CLLocationCoordinate2D center;
//    center.latitude = user2.latitude;
//    center.longitude = user2.longitude;
//    
//    MKCoordinateSpan span;
//    span.latitudeDelta = 0.01f;
//    span.longitudeDelta = 0.01f;
//    myregion.center = center;
//    myregion.span = span;
//    
//    [mapview setRegion:myregion animated:YES];
//    
//    CLLocationCoordinate2D restolocation;
//    restolocation.latitude = user2.latitude;
//    restolocation.longitude = user2.longitude;
//    
//    Annotation *myannotation = [Annotation alloc];
//    myannotation.coordinate = restolocation;
//    myannotation.title = user2.name;
//    
//    [self.mapview addAnnotation:myannotation];
    
}


-(void)viewDidLoad
{
    [super viewDidLayoutSubviews];
    [self.scroller layoutIfNeeded];
    self.scroller.contentSize = self.Contentview.bounds.size;
    self.scroller.contentSize = CGSizeMake(320.0f, 2600.0f);
    
    mondaytime.lineBreakMode = NSLineBreakByWordWrapping;
    mondaytime.numberOfLines = 0;
    tuesdaytime.lineBreakMode = NSLineBreakByWordWrapping;
    tuesdaytime.numberOfLines = 0;
    wednesdaytime.lineBreakMode = NSLineBreakByWordWrapping;
    wednesdaytime.numberOfLines = 0;
    thursdaytime.lineBreakMode = NSLineBreakByWordWrapping;
    thursdaytime.numberOfLines = 0;
    fridaytime.lineBreakMode = NSLineBreakByWordWrapping;
    fridaytime.numberOfLines = 0;
    saturdaytime.lineBreakMode = NSLineBreakByWordWrapping;
    saturdaytime.numberOfLines = 0;
    sundaytime.lineBreakMode = NSLineBreakByWordWrapping;
    sundaytime.numberOfLines = 0;
    addresslbl.lineBreakMode = NSLineBreakByWordWrapping;
    addresslbl.numberOfLines = 0;
    }

//-(void)viewDidLayoutSubviews
//{
////    [super viewDidLayoutSubviews];
////  //  [self.scroller layoutIfNeeded];
////    self.scroller.contentSize = self.contentview.bounds.size;
////    self.scroller.contentSize = CGSizeMake(320.0f,4000.0f);
//}
//-(void)viewDidAppear:(BOOL)animated{
//    [super viewDidLayoutSubviews];
//  //  [self.scroller layoutIfNeeded];
//    self.scroller.contentSize = self.contentview.bounds.size;
//    self.scroller.contentSize = CGSizeMake(320.0f,2500.0f);
//
//
//
//
//}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scroller layoutIfNeeded];
    self.scroller.contentSize = self.Contentview.bounds.size;
    self.scroller.contentSize = CGSizeMake(320.0f, 2600.0f);
    //Checkintable.frame = CGRectMake(Checkintable.frame.origin.x, Checkintable.frame.origin.y, Checkintable.frame.size.width, Checkintable.contentSize.height);
}

-(void)getdata
{
    user2 = [SUser new];
    user2.address = [myDict objectForKey:@"address"];
    user2.telephone1 = [myDict objectForKey:@"telephone1"];
    user2.latitude = [[myDict objectForKey:@"latitude"]doubleValue];
    user2.longitude = [[myDict objectForKey:@"longitude"]doubleValue];
    
    
    self.addresslbl.text = user2.address;
    self.contactlbl.text = user2.telephone1;
}
-(void)outletimage1
{
    NSDictionary *image1Dict = [myDict objectForKey:@"outletImage"];
     [mobilethumbimageArray removeAllObjects];
    for (id jsonData in  image1Dict)
    {
        user2.restoimages = [jsonData objectForKey:@"image"];
        
            user2.mobilepath = [ user2.restoimages valueForKey:@"mobileThumbImagePath"];
            user2.mobilethumbpath =[ user2.restoimages valueForKey:@"mobilePath"];
        user2.originalimagepath = [user2.restoimages valueForKey:@"originalImagePath"];
        [mobileimageArray removeAllObjects];

    if  (!mobileimageArray) mobileimageArray = [[NSMutableArray alloc] init];
            [mobileimageArray addObject:[NSString stringWithFormat:@"http://167.114.0.116:9090/%@",user2.mobilepath]];
        
       
        if  (!mobilethumbimageArray) mobilethumbimageArray = [[NSMutableArray alloc] init];
        [mobilethumbimageArray addObject:[NSString stringWithFormat:@"http://167.114.0.116:9090/%@",user2.mobilethumbpath]];
        if (!originalimagesArray) originalimagesArray = [[NSMutableArray alloc]init];
        {
            [originalimagesArray addObject:[NSString stringWithFormat:@"http://167.114.0.116:9090/%@",user2.originalimagepath]];
        }
    }
}
#pragma mark -restaurant timing
-(void)mondaytiming
{
    NSDictionary *mondaytmng = [myDict objectForKey:@"mondayTimingSlabs"];
    NSString *mystring = @"";
    NSString *a = nil;
    
    for (id jsonData in mondaytmng)
    {
        user2.startTime = [jsonData objectForKey:@"startTime"];
        user2.endTime = [jsonData objectForKey:@"endTime"];
        
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *formattedDateString = [dateFormatter dateFromString:user2.startTime];
        NSDate *formattedDateString1 = [dateFormatter dateFromString:user2.endTime];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];
        NSString *formateddate = [dateFormatter1 stringFromDate:formattedDateString];
        NSString *formateddate1 = [dateFormatter1 stringFromDate:formattedDateString1];
        
        a = [NSString stringWithFormat:@"%@ - %@",formateddate,formateddate1];
        mystring = [NSString stringWithFormat:@" %@ %@,",mystring,a];
       }
    
    
    self.mondaytime.text = mystring;
    
}
-(void)tuesdaytiming
{
    NSDictionary *tuesdaytmng = [myDict objectForKey:@"tuesdayTimingSlabs"];
    NSString *mystring = @"";
    NSString *a = nil;
    for (id jsonData in tuesdaytmng)
    {
        user2.startTime = [jsonData objectForKey:@"startTime"];
        user2.endTime = [jsonData objectForKey:@"endTime"];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *formattedDateString = [dateFormatter dateFromString:user2.startTime];
        NSDate *formattedDateString1 = [dateFormatter dateFromString:user2.endTime];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];
        NSString *formateddate = [dateFormatter1 stringFromDate:formattedDateString];
        NSString *formateddate1 = [dateFormatter1 stringFromDate:formattedDateString1];
        
        a = [NSString stringWithFormat:@"%@ - %@",formateddate,formateddate1];
        mystring = [NSString stringWithFormat:@" %@ %@,",mystring,a];
    }
    self.tuesdaytime.text = mystring;
}
-(void)wednesdaytiming
{
    NSDictionary *wednesdaytmng = [myDict objectForKey:@"wednesdayTimingSlabs"];
    NSString *mystring = @"";
    NSString *a = nil;
    for (id jsonData in wednesdaytmng)
    {
        user2.startTime = [jsonData objectForKey:@"startTime"];
        user2.endTime = [jsonData objectForKey:@"endTime"];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *formattedDateString = [dateFormatter dateFromString:user2.startTime];
        NSDate *formattedDateString1 = [dateFormatter dateFromString:user2.endTime];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];
        NSString *formateddate = [dateFormatter1 stringFromDate:formattedDateString];
        NSString *formateddate1 = [dateFormatter1 stringFromDate:formattedDateString1];
        
        a = [NSString stringWithFormat:@"%@ - %@",formateddate,formateddate1];
        mystring = [NSString stringWithFormat:@" %@ %@,",mystring,a];    }
    self.wednesdaytime.text = mystring;
}
-(void)thursdaytiming
{
    NSDictionary *thursdaytmng = [myDict objectForKey:@"thrusdayTimingSlabs"];
    NSString *mystring = @"";
    NSString *a = nil;
    for (id jsonData in thursdaytmng)
    {
        user2.startTime = [jsonData objectForKey:@"startTime"];
        user2.endTime = [jsonData objectForKey:@"endTime"];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *formattedDateString = [dateFormatter dateFromString:user2.startTime];
        NSDate *formattedDateString1 = [dateFormatter dateFromString:user2.endTime];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];
        NSString *formateddate = [dateFormatter1 stringFromDate:formattedDateString];
        NSString *formateddate1 = [dateFormatter1 stringFromDate:formattedDateString1];
        
        a = [NSString stringWithFormat:@"%@ - %@",formateddate,formateddate1];
        mystring = [NSString stringWithFormat:@" %@ %@,",mystring,a];
    }
    self.thursdaytime.text = mystring;
}
-(void)fridaytiming
{
    NSDictionary *fridaytmng = [myDict objectForKey:@"fridayTimingSlabs"];
    NSString *mystring = @"";
    NSString *a = nil;
    for (id jsonData in fridaytmng)
    {
        user2.startTime = [jsonData objectForKey:@"startTime"];
        user2.endTime = [jsonData objectForKey:@"endTime"];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *formattedDateString = [dateFormatter dateFromString:user2.startTime];
        NSDate *formattedDateString1 = [dateFormatter dateFromString:user2.endTime];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];
        NSString *formateddate = [dateFormatter1 stringFromDate:formattedDateString];
        NSString *formateddate1 = [dateFormatter1 stringFromDate:formattedDateString1];
        
        a = [NSString stringWithFormat:@"%@ - %@",formateddate,formateddate1];
        mystring = [NSString stringWithFormat:@" %@ %@,",mystring,a];    }
    self.fridaytime.text = mystring;
}
-(void)saturdaytiming
{
    NSDictionary *saturdaytmng = [myDict objectForKey:@"saturdayTimingSlabs"];
    NSString *mystring = @"";
    NSString *a = nil;
    for (id jsonData in saturdaytmng)
    {
        user2.startTime = [jsonData objectForKey:@"startTime"];
        user2.endTime = [jsonData objectForKey:@"endTime"];
        NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *formattedDateString = [dateFormatter dateFromString:user2.startTime];
        NSDate *formattedDateString1 = [dateFormatter dateFromString:user2.endTime];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];
        NSString *formateddate = [dateFormatter1 stringFromDate:formattedDateString];
        NSString *formateddate1 = [dateFormatter1 stringFromDate:formattedDateString1];
        
        a = [NSString stringWithFormat:@"%@ - %@",formateddate,formateddate1];
        mystring = [NSString stringWithFormat:@" %@ %@,",mystring,a];
    }
    self.saturdaytime.text = mystring;
}
-(void)sundaytiming
{
    NSDictionary *sundaytmng = [myDict objectForKey:@"sundayTimingSlabs"];
    NSString *mystring = @"";
    NSString *a = nil;
   
    for (id jsonData in sundaytmng)
    {
        user2.startTime = [jsonData objectForKey:@"startTime"];
        user2.endTime = [jsonData objectForKey:@"endTime"];
       // NSDate *date = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        NSDate *formattedDateString = [dateFormatter dateFromString:user2.startTime];
        NSDate *formattedDateString1 = [dateFormatter dateFromString:user2.endTime];
        
        NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
        [dateFormatter1 setDateFormat:@"hh:mm a"];
        NSString *formateddate = [dateFormatter1 stringFromDate:formattedDateString];
        NSString *formateddate1 = [dateFormatter1 stringFromDate:formattedDateString1];
        
        a = [NSString stringWithFormat:@"%@ - %@",formateddate,formateddate1];
        mystring = [NSString stringWithFormat:@" %@ %@,",mystring,a];
    }
    self.sundaytime.text = mystring;
}

#pragma mark -Facilities
-(void)booleanmethod
{
    BOOL test1 = [[myDict valueForKey:@"ac"]boolValue];
    if (test1)
    {
         self.acimage.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.acimage.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test2 =[[myDict valueForKey:@"nonvegeterian"]boolValue];
    if (test2)
    {
        self.nonvegetarian.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.nonvegetarian.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test3 =[[myDict valueForKey:@"happyHours"]boolValue];
    if (test3)
    {
         self.happyhoursimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.happyhoursimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test4 =[[myDict valueForKey:@"disablFacility"]boolValue];
    if (test4)
    {
        self.disabledfriendlyimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.disabledfriendlyimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    
    BOOL test5 = [[myDict valueForKey:@"privatePartyArea"]boolValue];
    if (test5)
    {
        self.pvtpartyimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.pvtpartyimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test6 = [[myDict valueForKey:@"parking"]boolValue];
    if (test6)
    {
        self.parkingimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.parkingimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];

    }
    BOOL test7 = [[myDict valueForKey:@"dinein"]boolValue];
    if (test7)
    {
        self.dineinavailableimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.dineinavailableimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];

    }
    BOOL test8 =[[myDict valueForKey:@"outdoorSeating"]boolValue];
    if (test8)
    {
        self.outdoorseatingimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.outdoorseatingimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test9 = [[myDict valueForKey:@"barArea"]boolValue];
    if (test9)
    {
        self.barareaimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.barareaimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test10 =[[myDict valueForKey:@"liveSportsBroadcast"]boolValue];
    if (test10)
    {
        self.livesportimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.livesportimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test11 =[[myDict valueForKey:@"open24_7"]boolValue];
    if (test11)
    {
        self.open24_7img.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.open24_7img.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test12 =[[myDict valueForKey:@"jainFood"]boolValue];
    if (test12)
    {
        self.jainfoodimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.jainfoodimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test13 =[[myDict valueForKey:@"buffetLunch"]boolValue];
    if (test13)
    {
        self.buffetlunchimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.buffetlunchimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test14 = [[myDict valueForKey:@"banquet"]boolValue];
    if (test14)
    {
        self.banquetimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.banquetimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test15 = [[myDict valueForKey:@"partyOrders"]boolValue];
    if (test15)
    {
        self.partyordersimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.partyordersimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test16 =[[myDict valueForKey:@"theme"]boolValue];
    if (test16)
    {
        self.themeimg.image = [UIImage imageNamed:@"done_click.png"];

    }
    else
    {
         self.themeimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test17 =[[myDict valueForKey:@"englishSpeakingWaiters"]boolValue];
    if (test17)
    {
        self.engspeakingimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.engspeakingimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test18 =[[myDict valueForKey:@"liveFoodCounter"]boolValue];
    if (test18)
    {
        self.livefoodimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.livefoodimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test19 =[[myDict valueForKey:@"acceptsCreditCard"]boolValue];
    if (test19)
    {
        self.creditcardimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.creditcardimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test20 =[[myDict valueForKey:@"servesAlcohol"]boolValue];
    if (test20)
    {
        self.alcoholimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.alcoholimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test21 =[[myDict valueForKey:@"petFriendly"]boolValue];
    if (test21)
    {
        self.petfriendly.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.petfriendly.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test22 = [[myDict valueForKey:@"wifi"]boolValue];
    if (test22)
    {
        self.wifiimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.wifiimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test23 =[[myDict valueForKey:@"smokingaAllowed"]boolValue];
    if (test23)
    {
        self.smokingimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.smokingimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test24 =[[myDict valueForKey:@"homeDelivery"]boolValue];
    if (test24)
    {
        self.provideshomedelivery.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.provideshomedelivery.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test25 =[[myDict valueForKey:@"tableReservation"]boolValue];
    if (test25)
    {
        self.tablereservimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.tablereservimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test26 =[[myDict valueForKey:@"corporateEvents"]boolValue];
    if (test26)
    {
        self.corporateimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.corporateimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test27 =[[myDict valueForKey:@"midnightBuffet"]boolValue];
    if (test27)
    {
        self.midnightbuffetimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.midnightbuffetimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test28 =[[myDict valueForKey:@"dressCode"]boolValue];
    if (test28)
    {
        self.dresscodeimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
       self.dresscodeimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test29 =[[myDict valueForKey:@"music"]boolValue];
    if (test29)
    {
        self.musicimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.musicimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test30 =[[myDict valueForKey:@"selfServices"]boolValue];
    if (test30)
    {
        self.selfserviceimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.selfserviceimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test31 =[[myDict valueForKey:@"takeAway"]boolValue];
    if (test31)
    {
        self.takeawayimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.takeawayimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test32 =[[myDict valueForKey:@"buffetDinner"]boolValue];
    if (test32)
    {
        self.buffetdinnerimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.buffetdinnerimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test33 =[[myDict valueForKey:@"midnightBuffet"]boolValue];
    if (test33)
    {
        self.midnightbuffetimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.midnightbuffetimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
 
    }
    BOOL test34 =[[myDict valueForKey:@"snacks"]boolValue];
    if (test34)
    {
        self.snacksimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.snacksimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test35 =[[myDict valueForKey:@"inMall"]boolValue];
    if (test35)
    {
        self.inmallimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.inmallimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test36 =[[myDict valueForKey:@"couponsAccepted"]boolValue];
    if (test36)
    {
        self.coupponsaccpted.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.coupponsaccpted.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test37 = [[myDict valueForKey:@"sundayBrunch"]boolValue];
    if (test37)
    {
        self.sundaybrunchimg.image = [UIImage imageNamed:@"done_click.png"];
    }
    else
    {
        self.sundaybrunchimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
    }
    BOOL test38 = [[myDict valueForKey:@"toilet"]boolValue];
    {
        if (test38)
        {
            self.restroomsimg.image = [UIImage imageNamed:@"done_click.png"];
        }
        else
        {
            self.restroomsimg.image = [UIImage imageNamed:@"Exit-Normal_Light.png"];
        }
    }
}

#pragma mark -collectionview Methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
numberOfItemsInSection:(NSInteger)section
{
    return mobilethumbimageArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"cell";
    
    CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSURL *url = [mobilethumbimageArray objectAtIndex:indexPath.row];
   [cell.restimages sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"default_144x144.png"]];
    
    cell.selected = YES;
    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionLeft];
    [cell.layer setBorderWidth:2.0f];
    [cell.layer setBorderColor:[UIColor whiteColor].CGColor];

      return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cell selected ");
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}
-(void)viewWillAppear:(BOOL)animated
{
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadInfo:) name:@"info" object:nil];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    if ([segue.identifier isEqualToString:@"showpicture"])
    {
        NSIndexPath *indexpath = [self.photocollectionview indexPathsForSelectedItems];
        
       ShowPicViewController *picVCobj = (ShowPicViewController *)segue.destinationViewController;
        //picVCobj.MyArray = [originalimagesArray objectAtIndex:indexpath.item];
        picVCobj.MyArray = originalimagesArray;
        
    }

}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation* loc = [locations lastObject]; // locations is guaranteed to have at least one object
    [NSThread sleepForTimeInterval:2];
    float latitude = loc.coordinate.latitude;
    float longitude = loc.coordinate.longitude;
    NSLog(@"%.8f",latitude);
    NSLog(@"%.8f",longitude);
    
    currentlat = latitude;
    currentlon = longitude;
}


- (IBAction)OpenMapBtn:(id)sender
{
    
    NSString *str = [NSString stringWithFormat:@"http://maps.apple.com/maps?saddr=%f,%f&daddr=%f,%f",currentlat, currentlon,user2.latitude,user2.longitude];
    NSString* webStringURL = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *URL = [NSURL URLWithString:webStringURL];
    [[UIApplication sharedApplication] openURL:URL];
}
@end
