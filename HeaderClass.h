//
//  HeaderClass.h
//  Gourmet7
//
//  Created by Mac Pro on 04/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderClass : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *myheader;

@end
