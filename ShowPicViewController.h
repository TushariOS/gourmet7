//
//  ShowPicViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 29/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowPicViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
@property (strong, nonatomic)NSArray *MyArray;

@end
