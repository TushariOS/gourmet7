//
//  SUserDB.m
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import "SUserDB.h"

#define kUserTableName @"UserLoginDetails"
#define kUserTableName1 @"RestaurantDetails"

@interface SUserDB ()
{
}
@end
@implementation SUserDB

+(SUserDB *)singleton {
    static dispatch_once_t pred;
    static SUserDB *shared;
        // Will only be run once, the first time this is called
    dispatch_once(&pred, ^{
        shared = [[SUserDB alloc] init];
    });
    return shared;
}
- (id) init
{
    self = [super init];
    if (self)
    {
        _db = [SDBManager defaultDBManager].dataBase;
    }
    return self;
}


- (void) createDataBase
{
    FMResultSet * set = [_db executeQuery:[NSString stringWithFormat:@"select count(*) from sqlite_master where type ='table' and name = '%@'",kUserTableName]];
    [set next];
    NSInteger count = [set intForColumnIndex:0];
    BOOL existTable = count;
    if (existTable)
    {
        NSLog(@"Database already exists!");
        self.callServiceStatus = @"Data Loaded";
    }
    else
    {
        NSString * query = @"CREATE TABLE LoginDetails ('role','token','firstName','lastName','loginId','userId','cityId','countryId','cityName','countryName')";
        BOOL resnews = [_db executeUpdate:query];
        if (!resnews)
        {
                       NSLog(@"Database creation failed");
            
            self.callServiceStatus = @"Error";
        }
        
        else
        {
            
           NSLog(@"Database created successfully");
           self.callServiceStatus = @"Load Data";
        }
        
        
    }
    NSString * query1 = @"CREATE TABLE CheckinDetails ('restoId','visitpurose','restoname','requestId')";
    BOOL resnews = [_db executeUpdate:query1];
    if (!resnews)
    {
        self.callServiceStatus = @"Error";
    }
    else
    {
        self.callServiceStatus = @"Load Data";
    }
}

//-(void)createDataBase1
//{
//    FMResultSet * set = [_db executeQuery:[NSString stringWithFormat:@"select count(*) from sqlite_master where type ='table' and name = '%@'",kUserTableName1]];
//    [set next];
//    NSInteger count = [set intForColumnIndex:0];
//    BOOL existTable1 = count;
//    if (existTable1)
//    {
//        NSLog(@"Database already exists!");
//        self.callServiceStatus = @"Data Loaded";
//    }
//    else
//    {
//        NSString * query1 = @"CREATE TABLE RestoDetails ('restoId','visitpurose','restoname','requestId')";
//        BOOL resnews1 = [_db executeUpdate:query1];
//        if (!resnews1)
//        {
//            // [AppDelegate showStatusWithText:@"Database creation fails" duration:2];
//            NSLog(@"Database creation failed");
//            //Throw error here retry creation
//            self.callServiceStatus = @"Error";
//
//    }
//        else
//        {
//            //[AppDelegate showStatusWithText:@"Database created successfully" duration:2];
//            NSLog(@"Database created successfully");
//            self.callServiceStatus = @"Load Data";
//        }
//}
//}
-(void) saveUserDetails:(SUser *)user
{
    [_db open];
    NSString * query = @"insert INTO LoginDetails ('role','token','firstName','lastName','loginId','userId','cityId','countryId','cityName','countryName') VALUES (?,?,?,?,?,?,?,?,?,?)";
    NSMutableArray * arguments = [[NSMutableArray alloc]init];
    //[arguments addObject:[NSNumber numberWithInt:user.ID]];
    [arguments addObject:user.role];
    [arguments addObject:user.token];
    [arguments addObject:user.firstName];
    [arguments addObject:user.lastName];
    [arguments addObject:user.loginId];
    [arguments addObject:user.userId];
    [arguments addObject:user.cityId];
    [arguments addObject:user.countryId];
    [arguments addObject:user.CityName];
    [arguments addObject:user.Countryname];
    
    [_db executeUpdate:query withArgumentsInArray:arguments];
     NSLog(@"arguments %@",arguments);
    [_db close];
}
-(void) updateUserCityName:(NSString *)usercity
{
    [_db open];
    NSString * query = [NSString stringWithFormat:@"UPDATE LoginDetails set cityName='%@'  WHERE token='%@'",usercity, [[SUserDB singleton]fetchTokenNumber]];
    if (![_db open]){
        NSLog(@"Failed to open database!");
    }
    
    [_db executeUpdate:query withArgumentsInArray:nil];
    NSLog(@"arguments %@",query);
    [_db close];
}
-(void) updateUserCityId:(NSString *)usercityId
{
    [_db open];
    NSString * query = [NSString stringWithFormat:@"UPDATE LoginDetails set cityId='%@'  WHERE token='%@'",usercityId, [[SUserDB singleton]fetchTokenNumber]];
    if (![_db open]){
        NSLog(@"Failed to open database!");
    }
    
    [_db executeUpdate:query withArgumentsInArray:nil];
    NSLog(@"arguments %@",query);
    [_db close];
}
//-(void) updateUserDetails:(SUser *)user
//{
//    [_db open];
//    NSString * query = @"UPDATE LoginDetails SET role = ''";
//
//    NSMutableArray * arguments = [[NSMutableArray alloc]init];
//    //[arguments addObject:[NSNumber numberWithInt:user.ID]];
//    ;
//    [arguments addObject:[[SUserDB singleton]fetchcitname]];
//    
//    
//    [_db executeUpdate:query withArgumentsInArray:arguments];
//    NSLog(@"arguments %@",arguments);
//    [_db close];
//}

-(void)saverestodetails:(SUser2 *)user2
{
    [_db open];
    NSString *query1 = @"insert INTO CheckinDetails ('restoId','visitpurose','restoname','requestId') VALUES (?,?,?,?)";
    NSMutableArray * arguments1 = [[NSMutableArray alloc]init];
    [arguments1 addObject:user2.RestoId];
    [arguments1 addObject:user2.visitpurpose];
    [arguments1 addObject:user2.Restoname];
    [arguments1 addObject:user2.RequestId];
    [_db executeUpdate:query1 withArgumentsInArray:arguments1];
    [_db close];
}
-(void) deleteAllRecords:(SUser *)user
{
    [_db open];
    NSString *query = @"DELETE FROM LoginDetails";
    [_db executeUpdate:query];
    NSLog(@"Row Deleted");
    [_db close];
}


-(NSString *) fetchTokenNumber
{
    [_db open];
    NSString * query = @"SELECT token from LoginDetails";
    FMResultSet * rs = [_db executeQuery:query];
    NSString *token = 0;
    while ([rs next])
    {
        token = [rs stringForColumn:@"token"];
    }
    [_db close];
    return token;
}
-(NSString *) fetchcityname
{
    [_db open];
    NSString * query = @"SELECT CityName from LoginDetails";
    FMResultSet * rs = [_db executeQuery:query];
    NSString *CityName = 0;
    while ([rs next])
    {
        CityName = [rs stringForColumn:@"CityName"];
    }
    [_db close];
    return CityName;
}
-(NSString *) fetchcityId
{
    [_db open];
    NSString * query = @"SELECT cityId from LoginDetails";
    FMResultSet * rs = [_db executeQuery:query];
    NSString *cityId = 0;
    while ([rs next])
    {
        cityId = [rs stringForColumn:@"cityId"];
    }
    [_db close];
    return cityId;
}
-(NSString *) fetchCountryId
{
    [_db open];
    NSString * query = @"SELECT countryId from LoginDetails";
    FMResultSet * rs = [_db executeQuery:query];
    NSString *countryId = 0;
    while ([rs next])
    {
        countryId = [rs stringForColumn:@"countryId"];
    }
    [_db close];
    return countryId;
}
-(NSString *) fetchCountryName
{
    [_db open];
    NSString * query = @"SELECT countryName from LoginDetails";
    FMResultSet * rs = [_db executeQuery:query];
    NSString *countryName = 0;
    while ([rs next])
    {
        countryName = [rs stringForColumn:@"countryName"];
    }
    [_db close];
    return countryName;
}

-(NSString *)fetchuserId
{
    [_db open];
    NSString * query1 = @"SELECT userId from LoginDetails";
    FMResultSet *rs1 = [_db executeQuery:query1];
    NSString *userId = 0;
    while ([rs1 next])
    {
        userId = [rs1 stringForColumn:@"userId"];
    }
    [_db close];
    return userId;
}
-(NSString *)fetchfirstname
{
    [_db open];
    NSString *query1 = @"SELECT firstName from LoginDetails";
    FMResultSet *rs1 = [_db executeQuery:query1];
    NSString *firstName = 0;
    while ([rs1 next])
    {
        firstName = [rs1 stringForColumn:@"firstName"];
    }
    [_db close];
    return firstName;
}
-(NSString *)fetchlastname
{
    [_db open];
    NSString *query1 = @"SELECT lastName from LoginDetails";
    FMResultSet *rs1 = [_db executeQuery:query1];
    NSString *lastName = 0;
    while ([rs1 next])
    {
        lastName = [rs1 stringForColumn:@"lastName"];
    }
    [_db close];
    return lastName;
}


-(NSString *)fetchvisitpurpose
{
    [_db open];
    NSString *query = @"SELECT visitpurose from CheckinDetails";
    FMResultSet *rs = [_db executeQuery:query];
    NSString * visitpurose = 0;
    while ([rs next])
    {
        visitpurose = [rs stringForColumn:@"visitpurose"];
        
    }
    [_db close];
    return visitpurose;
}


@end