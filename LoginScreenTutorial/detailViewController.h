//
//  detailViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 17/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUser.h"
#import <Foundation/Foundation.h>
#import "RateView.h"
#import "CheckinViewController.h"
#import "SWRevealViewController.h"



@interface detailViewController : UIViewController <RateViewDelegate,CheckinViewControllerDelegate,SWRevealViewControllerDelegate>

{
        int btn;
    bool Checkin,follower;
    BOOL gen,myspace, info, premium;
    UIView *subview;
}
@property (weak, nonatomic) IBOutlet UIImageView *restoimage;
@property (weak, nonatomic) IBOutlet UIImageView *restoimage1;
@property (weak, nonatomic) IBOutlet UILabel *mondaytime;
- (IBAction)showrate:(id)sender;
@property (strong, nonatomic)IBOutlet UIBarButtonItem *sidebarbutton;
@property (weak, nonatomic) IBOutlet UILabel *RestoNamelbl;
@property (weak, nonatomic) IBOutlet UILabel *Arealbl;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic) NSString *restoId;
@property (weak, nonatomic) IBOutlet UILabel *discountlbl;
@property (weak, nonatomic) IBOutlet UILabel *specialitylbl;
@property (weak, nonatomic) IBOutlet UIButton *followbtn;
@property (weak, nonatomic) IBOutlet UIView *container1;
@property (weak, nonatomic) IBOutlet UIView *container2;
@property (weak, nonatomic) IBOutlet UIView *container3;
@property (weak, nonatomic) IBOutlet UIView *container4;
@property (weak, nonatomic) IBOutlet UILabel *membershiplbl;
@property (weak, nonatomic) IBOutlet UILabel *loyatypointslbl;
@property (strong, nonatomic) SUser *user;
@property (nonatomic, copy) NSDictionary *resaurantobject;
@property (strong, nonatomic)NSDictionary *MyDict;
@property (nonatomic, strong) NSArray *imageArray;
@property (nonatomic, strong) NSMutableArray *Arrayofimages;
@property (nonatomic, strong) NSMutableArray *Arrayofimages2;
@property (weak, nonatomic) IBOutlet RateView *rating;
@property (strong, nonatomic) IBOutlet UIView *detailuiview;
@property (strong, nonatomic) UIView *subview;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *requestId;
@property (strong, nonatomic) NSString *visitpurpose;
@property (strong, nonatomic)  NSDictionary *CheckoutData;
@property (strong, nonatomic)NSString *phonenum;
@property(nonatomic) BOOL active;
@property (nonatomic)BOOL follower;
@property (strong, nonatomic)NSDictionary *userInfo;


@property (strong, nonatomic) IBOutlet UILabel *check;
@property (weak, nonatomic) IBOutlet UIButton *checkinbtn;
@property (weak, nonatomic) IBOutlet UIButton *checkoutbtn;
- (IBAction)checkout:(id)sender;

//- (IBAction)Checkinout:(id)sender;
- (IBAction)changeview:(UISegmentedControl *)sender;
@property (weak, nonatomic) IBOutlet UIButton *genbutton;

@property (weak, nonatomic) IBOutlet UIButton *infobutton;
@property (weak, nonatomic) IBOutlet UIButton *prembutton;
@property (weak, nonatomic) IBOutlet UIButton *myspacebutton;
- (IBAction)MakeCall:(id)sender;

- (IBAction)submitbutton:(id)sender;
-(IBAction)Back_btn:(id)sender;


- (IBAction)infobtn:(id)sender;
- (IBAction)generalbtn:(id)sender;
- (IBAction)premiumbtn:(id)sender;
- (IBAction)myspacebtn:(id)sender;

@end
