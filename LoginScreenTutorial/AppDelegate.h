//
//  AppDelegate.h
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
+(NSString*)getNumber:(NSString*)str;

@end
