//
//  RegisterViewController.m
//  
//
//  Created by Mac Pro on 05/02/16.
//
//

#import "RegisterViewController.h"
#import "SUser.h"
#import "SUserDB.h"
#import "RestaurantListing.h"
#import "AFNetworking.h"
#import "DashboardViewController.h"
#define kOFFSET_FOR_KEYBOARD 80.0;
@interface RegisterViewController ()

@end

@implementation RegisterViewController
@synthesize FirstName,LastName,EmailID,LoginID,Password,ConfirmPassword,MobileNo,PreferredCity,ResidentCity,FirstNme,LastNme,MailID,Login,Passcode,ConfmPass,Mobilenumber,PrefferedCity,ResidentialCity,myscroller,Registerbutton,Bysigningup,customView;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.FirstName.delegate = self;

    self.LastName.delegate = self;

    self.EmailID.delegate = self;

    self.LoginID.delegate = self;

    self.Password.delegate = self;

    self.ConfirmPassword.delegate = self;

    self.MobileNo.delegate = self;
    self.PreferredCity.delegate = self;
    self.ResidentCity.delegate = self;
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardDidShowNotification object:nil];
    
  //  [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];


}

-(void)RegisterUrl
{
    
    
   
    NSString *corporateId = @"0";
    
    NSString *language = @"ENGLISH";
    
    NSString *timezone = @"";
    
    NSString *phone = @"";
    NSString *addressLine1 = @"";
    
    NSString *addressLine2 = @"";
    NSString *residentCity = @"0";
    NSString* residentCountry = @"0";
    NSString *pincode = @"";
    
    NSString *address = @"";
    NSString *anniversary =@"";
    NSString *birthDate = @"";
    
    NSString *claimEmail = @"";
    NSString *promocode = @"";
    
    NSString *isAddMember = @"";
    
    NSString *agreedToTermsOfUse = @"true";
    
    NSString* role = @"7";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    NSString *URLString = [NSString stringWithFormat:@"http://167.114.0.116:9090/Member/AddMember"];
    NSDictionary *params = @{@"firstName": self.FirstName.text,
                             @"lastName": self.LastName.text,
                             @"loginId":self.LoginID.text,
                             @"emailId":self.EmailID.text,
                             @"password":self.Password.text,
                             @"corporateId":corporateId,
                             @"mobile":self.MobileNo.text,
                             @"language":language,
                             @"timezone":timezone,
                             @"phone":phone,
                             @"addressLine1":addressLine1,
                             @"addressLine2":addressLine2,
                             @"residentCity":residentCity,
                             @"residentCountry":residentCountry,
                             @"pincode":pincode,
                             @"address":address,
                             @"anniversary":anniversary,
                             @"birthDate":birthDate,
                             @"claimEmail":claimEmail,
                             @"promocode":promocode,
                             @"isAddMember":isAddMember,
                             @"agreedToTermsOfUse":agreedToTermsOfUse,
                             @"role":role};
    
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         [self fetchresponse:responseObject];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];

}
-(void)fetchresponse:(NSDictionary *)jsonData
{
    
NSInteger success = 0;
    SUser *data1= [SUser new];
        success = [jsonData[@"membershiptype"] integerValue];
        data1.token=jsonData[@"token"];
        data1.role = jsonData[@"role"] ;
        data1.firstName = jsonData [@"firstName"];
        data1.lastName = jsonData [@"lastName"];
        data1.loginId = jsonData [@"loginId"];
        data1.userId = jsonData [@"userId"];
        data1.cityId = jsonData [@"cityId"];
        data1.countryId = jsonData [@"countryId"];
    NSString *ErrorMsg = [jsonData valueForKey:@"errorCode"];
    
    if (ErrorMsg ==nil)
    {
        [[SUserDB singleton]saveUserDetails:data1];
    }
    else
    {
        
    }

              NSLog(@"JsonData: %@",jsonData);
        NSLog(@"Token: %@",data1.token);
        NSLog(@"Success: %ld",(long)success);
    if([[self.LoginID text]  isEqual:data1.loginId])
    {
        
        DashboardViewController *dash = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
        //dash.mycity = currentcity;
        [self.navigationController pushViewController:dash animated:YES];
        NSLog(@"Login SUCCESS");
    }
    
    else
    {
        //[self alertStatus:@"Please Enter Correct Login Id and Password!":@"LogIn Failed":0];
    }


    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (IBAction)RegisterBtn:(id)sender
{
    NSString *msg;
    
     if ([FirstName.text length] == 0)
    {
        msg=@"Please enter first name";
    }else if ([LastName.text length]== 0)
    {
        msg = @"Please enter last name";
    }
    else if([EmailID.text length]== 0)
    {
        msg = @"Please enter emailid";
    }
    if([LoginID.text length]==0)
    {
        msg =@"Please enter loginid";
        
    }
    else if ([Password.text length]== 0)
    {
        msg = @"Please enter password";
    }
    else if ([ConfirmPassword.text length]== 0)
    {
        msg = @"Please Re-enter password";
    }
       else if ([Password.text isEqualToString: ConfirmPassword.text])
       {
       
       }
    else
    {
        msg =@"Password did not match";
    }
    
    if ([ self NSStringIsValidEmail:[EmailID text]] == 1)
    {
        
    }else
    {
        msg=@"Enter valid EmailId";
    }
    
    [self RegisterUrl];
msg =@"Registered Successfully";


}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
   [self.view endEditing:YES];
    return YES;
}
//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    [self.view endEditing:YES];
//}


//- (void)registerForKeyboardNotifications
//{
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWasShown:)
//                                                 name:UIKeyboardDidShowNotification
//                                               object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(keyboardWillBeHidden:)
//                                                 name:UIKeyboardWillHideNotification
//                                               object:nil];
//    
//}
//- (void)deregisterFromKeyboardNotifications {
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardDidHideNotification
//                                                  object:nil];
//    
//    [[NSNotificationCenter defaultCenter] removeObserver:self
//                                                    name:UIKeyboardWillHideNotification
//                                                  object:nil];
//    
//}
//- (void)viewWillAppear:(BOOL)animated {
//    
//    [super viewWillAppear:animated];
//   
//    [self registerForKeyboardNotifications];
//    
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    
//    [self deregisterFromKeyboardNotifications];
//    
//    [super viewWillDisappear:animated];
//    
//}
//- (void)keyboardWasShown:(NSNotification *)notification {
//    
//    NSDictionary* info = [notification userInfo];
//    
//    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    CGPoint buttonOrigin =Bysigningup.frame.origin;
//    
//    CGFloat buttonHeight = Bysigningup.frame.size.height;
//    
//    CGRect visibleRect =self.view.frame;
//    
//    visibleRect.size.height -= keyboardSize.height;
//    
//    if (!CGRectContainsPoint(visibleRect, buttonOrigin))
//    {
//        
//        CGPoint scrollPoint = CGPointMake(0.0, buttonOrigin.y - visibleRect.size.height + buttonHeight);
//        
//        [self.myscroller setContentOffset:scrollPoint animated:YES];
//        
//    }
//    
//}
//
//- (void)keyboardWillBeHidden:(NSNotification *)notification {
//    
//    [self.myscroller setContentOffset:CGPointZero animated:YES];
//    self.myscroller.scrollEnabled = NO;
//}
//
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
//    return YES;
//}
//
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//    
//    [self.view endEditing:YES];
//    return YES;
//}
//
//
//- (void)keyboardDidShow:(NSNotification *)notification
//{
//    // Assign new frame to your view
//    [self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
//    
//}
//
//-(void)keyboardDidHide:(NSNotification *)notification
//{
//    [self.view setFrame:CGRectMake(0,0,320,460)];
//}

//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
//    return YES;
//}
//
//
//- (BOOL)textFieldShouldEndEditing:(UITextField *)textField {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
//    
//    [self.view endEditing:YES];
//    return YES;
//}
//
//
//- (void)keyboardDidShow:(NSNotification *)notification
//{
//    // Assign new frame to your view
//    [self.view setFrame:CGRectMake(0,-110,320,460)]; //here taken -110 for example i.e. your view will be scrolled to -110. change its value according to your requirement.
//    
//}
//
//-(void)keyboardDidHide:(NSNotification *)notification
//{
//    [self.view setFrame:CGRectMake(0,0,320,460)];
//}
- (void)keyboardWillShow:(NSNotification*)aNotification {
    [UIView animateWithDuration:0.25 animations:^
     {
         CGRect newFrame = [customView frame];
         newFrame.origin.y -= 50; // tweak here to adjust the moving position
         [customView setFrame:newFrame];
         
     }completion:^(BOOL finished)
     {
         
     }];
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
//    [UIView animateWithDuration:0.25 animations:^
//     {
//         CGRect newFrame = [customView frame];
//         newFrame.origin.y += 50; // tweak here to adjust the moving position
//         [customView setFrame:newFrame];
//         
//     }completion:^(BOOL finished)
//     {
//         
//     }];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.customView layoutIfNeeded];
    self.myscroller.contentSize = self.customView.bounds.size;
    self.myscroller.contentSize = CGSizeMake(320.0f, 800.0f);
    //Checkintable.frame = CGRectMake(Checkintable.frame.origin.x, Checkintable.frame.origin.y, Checkintable.frame.size.width, Checkintable.contentSize.height);
}
- (IBAction)GetOTP:(id)sender
{
    
}
- (IBAction)TermsServices:(id)sender {
}
@end
