//
//  ViewController.h
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUser.h"
#import "RegisterViewController.h"
#import "SWRevealViewController.h"
#import "DashboardViewController.h"
#import "SidemenuViewController.h"

@interface ViewController : UIViewController <UITextFieldDelegate,SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) NSString *txtClient;
@property (weak, nonatomic) NSString *txtURL;
@property (weak, nonatomic) NSString *txtRole;
@property (weak, nonatomic) NSString *token;
@property (strong, nonatomic)NSString *currentcity;
@property (strong, nonatomic) SUser *data;
@property (strong, nonatomic)NSString *error_msg;
- (IBAction)backgroundTap:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *Loginpatchview;
@property (weak, nonatomic) IBOutlet UIButton *Registerbutton;

@property (weak, nonatomic) IBOutlet UIImageView *loginpatch;

- (IBAction)Registerbtn:(id)sender;
@end
