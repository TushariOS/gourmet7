//
//  SidemenuViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 11/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "ViewController.h"
#import "SidemenuViewController.h"
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "SDBManager.h"
#import "ViewController.h"
#import "SUser.h"
#import "SUserDB.h"

@interface SidemenuViewController ()

@end

@implementation SidemenuViewController
{
    NSArray *menuItems;
}
@synthesize userName,Password,data;
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    menuItems = @[@"Dashboard", @"RestaurantList", @"Filter", @"Logout", @"About", @"Help"];
   
    // Do any additional setup after loading the view.
}


-(void)catchresponse:(NSDictionary*)mydict
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return menuItems.count;
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LoginView"])
    {
        ViewController *destVCobj =  (ViewController *) segue.destinationViewController;
        //  detailViewController *destVCobj = segue.destinationViewController;
       [self.navigationController pushViewController:destVCobj animated:YES];
        
    }
    
   }

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [menuItems objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row==3)
    {
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"USER_ID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"USER_PASSWORD"];
        [[NSUserDefaults standardUserDefaults] synchronize];
       //  [[SUserDB singleton]deleteAllRecords:data];
      
    }
}
@end
