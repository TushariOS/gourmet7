//
//  SUserDB.h
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import "SDBManager.h"
#import "SUser.h"
#import "SUser2.h"
@protocol RetieveUpdatedValues;

@interface SUserDB : NSObject {
    FMDatabase * _db;
     //id currentDelegate_;
}
- (void) createDataBase;
- (NSString *) fetchMaxNewsDate;
- (NSString *) fetchMaxEventDate;
- (NSString *) fetchTokenNumber;
-(NSString *) fetchcityname;
-(NSString *)fetchuserId;
-(NSString *)fetchvisitpurpose;
-(NSString *)fetchfirstname;
-(NSString *)fetchlastname;
-(NSString *) fetchcityId;
-(NSString *) fetchCountryId;
-(NSString *) fetchCountryName;

-(void) updateUserCityId:(NSString *)usercityId;
-(void) updateUserCityName:(NSString *)usercity;
- (void) saveUserNews:(SUser *) user;
- (void) saveUserEvents:(SUser *) user;
- (void) saveUserDetails:(SUser *) user;
- (void) updateUserDetails:(SUser *)user;
- (void) deleteAllRecords:(SUser *) user;
- (void) deleteUserWithId:(NSString *) uid;
- (void)saverestodetails:(SUser2 *)user2;
- (void) mergeWithUser:(SUser *) user action: (NSString *)actionPerformed newGroup :(NSString *)newGroup;
- (NSArray *) findWithUid:(NSString *) uid limit:(int) limit;

+(SUserDB *)singleton;
- (NSArray *) getBackup;
- (NSArray *) getGroupYuDORecords;
- (void) fetchLoginDetails:(SUser *) user;
- (void) fetchUserDetails:(SUser *) user;
- (void)fetchrestodetails:(SUser2 *)user2;
- (NSMutableArray *) fetchQueueDataNews;
- (NSMutableArray *) fetchQueueDataEvents;
- (NSMutableArray *)getExistingData :(NSString *)ID;
- (NSMutableArray *)getExistingEventData :(NSString *)ID;
- (NSMutableArray *)getExistingNewsData :(NSString *)ID;
- (NSMutableArray *)Sync;

//@property(readwrite)  NSTimeInterval nextEpoch;


@property (retain, nonatomic)  NSString * callServiceStatus;
@property (assign, nonatomic) id <RetieveUpdatedValues>delegate;
@end
@protocol RetieveUpdatedValues<NSObject>
@optional
- (void)sendUpdates:(NSArray *)updates;


@end