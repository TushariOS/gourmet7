//
//  SDBManager.m
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import "SDBManager.h"
#import "FMDatabase.h"

#define kDefaultDBName @"LoginDetails.sqlite"

@interface SDBManager ()

@end

@implementation SDBManager

static SDBManager * _sharedDBManager;

+ (SDBManager *) defaultDBManager {
	if (!_sharedDBManager) {
		_sharedDBManager = [[SDBManager alloc] init];
	}
	return _sharedDBManager;
}
- (void) dealloc
{
    [self close];
}
- (id) init
{
    self = [super init];
    if (self) {
        int state = [self initializeDBWithName:kDefaultDBName];
        if (state == -1) {
            NSLog(@"Database initialization failed");
        } else {
            NSLog(@"Database initialization success");
        }
    }
    return self;
}
- (int) initializeDBWithName : (NSString *) name {
    if (!name) {
		return -1;
	}
    NSString * docp = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask, YES) objectAtIndex:0];
	_name = [docp stringByAppendingString:[NSString stringWithFormat:@"/%@",name]];
	NSFileManager * fileManager = [NSFileManager defaultManager];
    BOOL exist = [fileManager fileExistsAtPath:_name];
    NSLog(@"DB path:%@",_name);
    [self connect];
    if (!exist) {
        return 0;
    } else {
        return 1;
        }
}
- (void) connect {
	if (!_dataBase) {
		_dataBase = [[FMDatabase alloc] initWithPath:_name];
	}
	if (![_dataBase open]) {
		NSLog(@"Can not open database");
	}
}
- (void) close {
	[_dataBase close];
    _sharedDBManager = nil;
}
@end
