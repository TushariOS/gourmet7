//
//  DashboardViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 11/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "DashboardViewController.h"
#import "SWRevealViewController.h"
#import "AFNetworking.h"
#import "SUserDB.h"
#import "DropdownlistViewController.h"

@interface DashboardViewController ()

@end

@implementation DashboardViewController
@synthesize mycity,currentcity,sidebarbutton,changecity,token,Listofcity;

- (void)viewDidLoad
{
    
    UIImage *logoimage = [UIImage imageNamed:@"logo_small.png"];
    
    token = [[SUserDB singleton]fetchTokenNumber];
    //NSString *city = [[SUserDB singleton]fetchcitname];
    
    self.navigationItem.titleView = [[UIImageView alloc]initWithImage:logoimage];
    
    [[changecity layer] setBorderWidth:2.0f];
    [[changecity layer] setBorderColor:[UIColor whiteColor].CGColor];
    [[changecity layer]setCornerRadius:4.0f];
    [super viewDidLoad];
    self.revealViewController.delegate = self;

    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarbutton setTarget: self.revealViewController];
        [self.sidebarbutton setAction: @selector( mysidebarAnimated:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    mycity = [NSString stringWithFormat:@"%@,%@",[[SUserDB singleton]fetchcityname],[[SUserDB singleton]fetchCountryName]];

    
    NSLog(@"mycity is :%@",mycity);
    self.currentcity.text = mycity;
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

-(void)makeservicecall
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    //manager.requestSerializer.timeoutInterval = 40;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    NSString *URLString = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth"];
    NSDictionary *params = @{@"url":@"/city/findCityCountryId/1",
                             @"token":token,
                             @"macId":@"",
                             };
    
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         [self catchresponse:responseObject];
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];

}
-(void)catchresponse:(NSArray*)jsondata
{
    
    if (jsondata!=nil)
    {
    DropdownlistViewController *Drop = [self.storyboard instantiateViewControllerWithIdentifier:@"DropDownList"];
    Drop.MyArray = jsondata;
    [self.navigationController pushViewController:Drop animated:YES];
}
}

- (IBAction)barbutton:(id)sender
{
    
}

- (IBAction)Changecity:(id)sender
{
    [self makeservicecall];

}

- (IBAction)restolist:(id)sender
{
    
}
@end
