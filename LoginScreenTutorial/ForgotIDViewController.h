//
//  ForgotIDViewController.h
//  
//
//  Created by Mac Pro on 06/02/16.
//
//

#import <UIKit/UIKit.h>

@interface ForgotIDViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *EmailID;
- (IBAction)SubmitBtn:(id)sender;

- (IBAction)GoBackBtn:(id)sender;

@end
