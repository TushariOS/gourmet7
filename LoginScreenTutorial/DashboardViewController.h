//
//  DashboardViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 11/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface DashboardViewController : UIViewController<SWRevealViewControllerDelegate>

@property (strong, nonatomic)NSString *mycity;
@property (weak, nonatomic) IBOutlet UILabel *currentcity;
@property (weak, nonatomic) IBOutlet UIButton *changecity;
@property (strong, nonatomic)NSString *token;
@property (strong, nonatomic)IBOutlet UIBarButtonItem *sidebarbutton;
@property (strong, nonatomic)NSDictionary *Listofcity;
- (IBAction)Changecity:(id)sender;

- (IBAction)restolist:(id)sender;
@end
