//
//  detailViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 17/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import "detailViewController.h"
#import "RestaurantListing.h"
#import "SUser.h"
#import "infoViewController.h"
#import "ViewController.h"
#import "SUserDB.h"
#import "UIImageView+WebCache.h"
#import "RateView.h"
#import "DirectRtingViewController.h"
#import "CheckoutRating.h"
#import "FeedbackViewController.h"
#import "GeneralViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AFNetworking.h"

@interface detailViewController ()
{
    NSMutableArray *arrayofrestodetail;
}
@end

@implementation detailViewController
@synthesize restoimage,RestoNamelbl,Arealbl,followbtn,user,mondaytime,resaurantobject,restoimage1,imageArray,Arrayofimages2,Arrayofimages,rating,detailuiview,token,restoId,subview,MyDict,check,checkinbtn,checkoutbtn,userId,requestId,visitpurpose,CheckoutData,active,userInfo,infobutton,genbutton,prembutton,myspacebutton,sidebarbutton,phonenum;
- (void)viewDidLoad
{
        [super viewDidLoad];
    UIImage *logoimage = [UIImage imageNamed:@"logo_small.png"];
    
    self.navigationItem.titleView = [[UIImageView alloc]initWithImage:logoimage];
    
    self.revealViewController.delegate = self;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarbutton setTarget: self.revealViewController];
        [self.sidebarbutton setAction: @selector( mysidebarAnimated:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
   restoId = user.outletId;
   token = [[SUserDB singleton]fetchTokenNumber];
    userId = [[SUserDB singleton]fetchuserId];
    [self callDetailservice];
    
    MyDict = resaurantobject;
    //[self outletimage1];
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = detailuiview.bounds;
    gradientLayer.colors = [NSArray arrayWithObjects:(id)[UIColor whiteColor].CGColor, (id)[UIColor redColor].CGColor, nil];
    gradientLayer.startPoint = CGPointMake(0.8f, 1.0f);
    gradientLayer.endPoint = CGPointMake(1.0f, 1.0f);
    detailuiview.layer.mask = gradientLayer;
    self.infobutton.layer.cornerRadius = 5;
    self.myspacebutton.layer.cornerRadius = 5;
    self.prembutton.layer.cornerRadius = 5;
    self.genbutton.layer.cornerRadius = 5;
    [self.infobutton.layer setBorderWidth:1.0];
    [self.myspacebutton.layer setBorderWidth:1.0];
    [self.genbutton.layer setBorderWidth:1.0];
    [self.prembutton.layer setBorderWidth:1.0];
    [[infobutton layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    [[prembutton layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    [[myspacebutton layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    [[genbutton layer] setBorderColor:[UIColor darkGrayColor].CGColor];
    [self setModalPresentationStyle:UIModalPresentationCurrentContext];
   }
-(void)PassvisitPurpose:(CheckinViewController *)CheckinViewController1 PassvisitPurpose:(NSString *)Purpose
{
    visitpurpose = Purpose;
}

-(void)PassId:(CheckinViewController *)CheckinViewController PassedId:(NSString *)ID
{
    requestId = ID;
    NSLog(@"passed id is %@",ID);
}
-(void)detailsurl
{
    NSString *detailsURL = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=%%2FRestaurant%%2FfetchRestaurantDetails%%2F%@%%2F%@&token=%@",restoId,userId, token];
    
}
- (IBAction)BackButtonClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
//    RestaurantListing *dash = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantListing"];
//    
//    [self.navigationController pushViewController:dash animated:YES];
//
}
-(IBAction)Back_btn:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)Displayimages

{
    NSURL *url = [Arrayofimages objectAtIndex:0];
    
 
       [self.restoimage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@""]];
    }
-(void)displaydata
{
    self.RestoNamelbl.text = user.name;
    self.Arealbl.text = user.areaName;
   NSLog(@"%@",user.address);
     NSLog(@"%@",user.areaName);
    NSLog(@"%@",user.addressLine2);
    if (user.todaysDiscount == nil || [user.todaysDiscount isKindOfClass:[NSNull class]])
    {
        self.discountlbl.text = @"0%";
    }
    else
    {
     self.discountlbl.text = [NSString stringWithFormat:@"%@%%",user.todaysDiscount];
    }
   
    self.specialitylbl.text = user.specialty;
    self.membershiplbl.text = user.outletMembershipType;
    self.rating.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.rating.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.rating.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.rating.rating = [user.avgRating floatValue];
    self.rating.editable = NO;
    self.rating.maxRating = 5;
    self.rating.delegate = self;
}
#pragma mark -calling service
-(void)callDetailservice
{
    
    NSString *detailsURL = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=%%2FRestaurant%%2FfetchRestaurantDetails%%2F%@%%2F%@&token=%@",restoId,userId, token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    [manager GET:detailsURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        [self parse:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];

}
#pragma mark -parsing data
-(void)parse:(NSDictionary *)response1
{
    userInfo = response1;
    //userInfo = [response1 objectForKey:@"generalOffers"];
     resaurantobject=[response1 objectForKey:@"restaurant"];
    NSDictionary *Discount1 = [resaurantobject objectForKey:@"todaysDiscount"];
     user = [[SUser alloc]init];
        user.name = [resaurantobject valueForKey: @"name"];
        user.todaysDiscount = [Discount1 valueForKey:@"discount"];
        user.complementary = [resaurantobject valueForKey:@"complementary" ];
        user.address = [resaurantobject valueForKey:@"address"];
        user.telephone1 = [resaurantobject valueForKey:@"telephone1"];
        user.specialty = [resaurantobject valueForKey:@"specialty"];
        user.addressLine2 = [resaurantobject valueForKey:@"addressLine2"];
        user.outletMembershipType = [resaurantobject valueForKey:@"outletMembershipType"];
        user.checkIns = [resaurantobject valueForKey:@"checkIns"];
        user.giftAMeal = [resaurantobject valueForKey:@"giftAMeal"];
        user.promocodes = [resaurantobject valueForKey:@"promocodes"];
        user.startTime = [resaurantobject valueForKey:@"startTime"];
        user.endTime =[resaurantobject valueForKey:@"endTime"];
        user.latitude = [[resaurantobject valueForKey:@"latitude"]doubleValue];
        user.longitude = [[resaurantobject valueForKey:@"longitude"]doubleValue];
        user.avgRating = [[resaurantobject valueForKey:@"avgRating"]stringValue];
        user.outletId = [[resaurantobject valueForKey:@"outletId"]stringValue];
        user.phonenumber = [resaurantobject valueForKey:@"telephone1"];
         active = [[resaurantobject valueForKey:@"follow"]boolValue];
        phonenum = user.phonenumber;
        [self displaydata];
        [self outletimage1];
    
    if (active)
    {
        [followbtn setBackgroundImage:[UIImage imageNamed:@"Followers-Click.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        [followbtn setBackgroundImage:[UIImage imageNamed:@"Followers-Normal02.png"] forState:UIControlStateNormal];
    }
       
        }
-(void)outletimage1
{
        NSDictionary *image1Dict = [resaurantobject objectForKey:@"logoImage"];
        user.mobilepath = [ image1Dict valueForKey:@"mobilePath"];
        user.mobilethumbpath =[image1Dict valueForKey:@"mobileThumbImagePath"];
        
        if  (!Arrayofimages) Arrayofimages = [[NSMutableArray alloc] init];
        [Arrayofimages addObject:[NSString stringWithFormat:@"http://167.114.0.116:9090/%@",user.mobilepath]];
        
        if  (!Arrayofimages2) Arrayofimages2 = [[NSMutableArray alloc] init];
        [Arrayofimages2 addObject:[NSString stringWithFormat:@"http://167.114.0.116:9090/%@",user.mobilethumbpath]];
    
    [self Displayimages];
    }
- (IBAction)buttonTapped:(UIButton *)sender
{
    NSLog(@"Button Tapped!");
}

- (IBAction)followbutton:(id)sender
{
    if(follower == true)
    {
        
        [followbtn setBackgroundImage:[UIImage imageNamed:@"Followers-Normal02.png"] forState:UIControlStateNormal];
        follower =false;
        
        
        }
    else
    {
        [followbtn setBackgroundImage:[UIImage imageNamed:@"Followers-Click.png"] forState:UIControlStateNormal];
        follower=true;
    }
    
    [self callfollowservice];
}
- (IBAction)callbtn:(id)sender {
}

- (IBAction)infobtn:(id)sender
{
    [prembutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [genbutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [myspacebutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [[infobutton layer] setBorderColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1].CGColor];
    [[prembutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[myspacebutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[genbutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [infobutton setTitleColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1] forState:UIControlStateNormal];
        info = YES;
    
    self.container1.hidden = YES;
    self.container2.hidden = YES;
    self.container3.hidden = YES;
    self.container4.hidden = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"info"object:nil userInfo:resaurantobject];
}


- (IBAction)changeview:(UISegmentedControl *)sender
{
    
    switch (sender.selectedSegmentIndex)
    {
        case 0:
        {
            self.container1.hidden = NO;
            self.container2.hidden = YES;
            self.container3.hidden = YES;
            self.container4.hidden = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"myspace"object:nil userInfo:userInfo];
        }
            break;
        case 1:
        {
            self.container1.hidden = YES;
            self.container2.hidden = NO;
            self.container3.hidden = YES;
            self.container4.hidden = YES;
        }
            break;
        case 2:
        {
            self.container1.hidden = YES;
            self.container2.hidden = YES;
            self.container3.hidden = NO;
            self.container4.hidden = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"general"object:nil userInfo:userInfo];
        }
            break;
            case 3:
        {
            self.container1.hidden = YES;
            self.container2.hidden = YES;
            self.container3.hidden = YES;
            self.container4.hidden = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"info"object:nil userInfo:resaurantobject];
        }
        default:
        {
            self.container1.hidden = YES;
            self.container2.hidden = YES;
            self.container3.hidden = YES;
            self.container4.hidden = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"info"object:nil userInfo:resaurantobject];
        }
            break;
            
    }}
- (IBAction)generalbtn:(id)sender
{
    
    [infobutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [prembutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [myspacebutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [genbutton setTitleColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1] forState:UIControlStateNormal];
    [[genbutton layer] setBorderColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1].CGColor];
    [[infobutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[myspacebutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[prembutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];

    
    self.container1.hidden = YES;
    self.container2.hidden = YES;
    self.container3.hidden = NO;
    self.container4.hidden = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"general"object:nil userInfo:userInfo];
 

}
- (IBAction)premiumbtn:(id)sender
{
    [infobutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [genbutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [myspacebutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [prembutton setTitleColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1] forState:UIControlStateNormal];
    [[prembutton layer] setBorderColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1].CGColor];
    [[infobutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[genbutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[myspacebutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    self.container1.hidden = YES;
    self.container2.hidden = NO;
    self.container3.hidden = YES;
    self.container4.hidden = YES;

}

- (IBAction)myspacebtn:(id)sender
{
    [infobutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [prembutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [genbutton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [myspacebutton setTitleColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1] forState:UIControlStateNormal];
    [[myspacebutton layer] setBorderColor:[UIColor colorWithRed:255/255.0 green:171/255.0 blue:78/255.0 alpha:1].CGColor];
    [[prembutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[infobutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    [[genbutton layer]setBorderColor:[UIColor darkGrayColor].CGColor];
    
    self.container1.hidden = NO;
    self.container2.hidden = YES;
    self.container3.hidden = YES;
    self.container4.hidden = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"myspace"object:nil userInfo:userInfo];


}

- (IBAction)Checkinout:(id)sender
{
  [check setText:[NSString stringWithFormat:@"Check-out"]];
    
    checkoutbtn.hidden = NO;
    checkinbtn.hidden = YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showcheckin"])
    {
        CheckinViewController *controller = (CheckinViewController *)segue.destinationViewController;
        controller.OutletId = restoId;
        
        controller.delegate = self;
    }
    
    if ([segue.identifier isEqualToString:@"showfeedback"])
    {
        
        FeedbackViewController *feed = (FeedbackViewController *)segue.destinationViewController;
        feed.outletId = restoId;
        NSLog(@"%@",feed.outletId);
        
    }
    if ([segue.identifier isEqualToString:@"showgeneral"])
    {
           }

     CheckinViewController*   newVC = segue.destinationViewController;
    [detailViewController setPresentationStyleForSelfController:self presentingController:newVC];
   
}

+ (void)setPresentationStyleForSelfController:(UIViewController *)selfController presentingController:(UIViewController *)presentingController
{
    if ([NSProcessInfo instancesRespondToSelector:@selector(isOperatingSystemAtLeastVersion:)])
    {
        //iOS 8.0 and above
        presentingController.providesPresentationContextTransitionStyle = YES;
        presentingController.definesPresentationContext = YES;
        
        [presentingController setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    }
    else
    {
        [selfController setModalPresentationStyle:UIModalPresentationCurrentContext];
        [selfController.navigationController setModalPresentationStyle:UIModalPresentationCurrentContext];
    }
}

-(void)checkouturl
{

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    NSString *urlstr = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth"];
    NSDictionary *params = @{@"id":requestId,
                             @"userId":userId,
                             @"token":token,
                             @"url":@"/DeviceServerPush/checkOut",
                             };

    [manager POST:urlstr parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self checkoutresponse:responseObject];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
    
}
-(void)checkoutresponse:(NSDictionary*)NewDict
{
    SUser *data1= [SUser new];
    
    data1.requestId = [NewDict valueForKey: @"requestId"];
    NSLog(@"JsonData: %@",data1.requestId);
    NSLog(@"Token: %@",data1.token);

if (NewDict !=nil)
{
    CheckoutRating *Rating = [self.storyboard instantiateViewControllerWithIdentifier:@"checkoutrating"];
    Rating.RateDict = NewDict;
    [self.navigationController pushViewController:Rating animated:YES];
    
}

}

-(void)callfollowservice
{
   
    NSString *follow;
    if (active == YES)
    {
        follow = @"false";
    }
    else
    {
        follow = @"true";
    }
    
    NSString *followurl = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=/Restaurant/FollowOutlet/%@/%@/%@&token=%@",userId,restoId,follow,token];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    [manager GET:followurl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSLog(@"JSON: %@", responseObject);
        
        user = [[SUser alloc]init];
        user.Active = [[responseObject valueForKey:@"Active"]boolValue];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
}

- (IBAction)checkout:(id)sender
{
    [self checkouturl];
    [check setText:[NSString stringWithFormat:@"Check-in"]];
    
    checkoutbtn.hidden = YES;
    checkinbtn.hidden = NO;
}
- (IBAction)showrate:(id)sender
{
    
    DirectRtingViewController *Check = [self.storyboard instantiateViewControllerWithIdentifier:@"DirectRating"];
    Check.modalPresentationStyle = UIModalPresentationFullScreen;
    Check.OutletId = restoId;
    NSLog(@"%@",Check.OutletId);
    [self.navigationController pushViewController:Check animated:YES];

}
- (IBAction)MakeCall:(id)sender
{
    NSString *tel = [NSString stringWithFormat:@"tel:%@",phonenum];
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:tel]];
}
@end
