//
//  RegisterViewController.h
//  
//
//  Created by Mac Pro on 05/02/16.
//
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *FirstName;
@property (weak, nonatomic) IBOutlet UITextField *LastName;
@property (weak, nonatomic) IBOutlet UITextField *EmailID;
@property (weak, nonatomic) IBOutlet UITextField *LoginID;
@property (weak, nonatomic) IBOutlet UITextField *Password;
@property (weak, nonatomic) IBOutlet UITextField *ConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *MobileNo;
@property (weak, nonatomic) IBOutlet UITextField *PreferredCity;
@property (weak, nonatomic) IBOutlet UITextField *ResidentCity;
@property (strong, nonatomic) IBOutlet UIView *customView;
@property (strong, nonatomic) NSString *FirstNme;
@property (strong, nonatomic) NSString *LastNme;
@property (strong, nonatomic) NSString *MailID;
@property (strong, nonatomic) NSString *Login;
@property (strong, nonatomic) NSString *Passcode;
@property (strong, nonatomic) NSString *ConfmPass;
@property (strong, nonatomic) NSString *Mobilenumber;
@property (weak, nonatomic) IBOutlet UILabel *Bysigningup;
@property (strong, nonatomic) NSString *PrefferedCity;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroller;
@property (weak, nonatomic) IBOutlet UIButton *Registerbutton;
@property (strong, nonatomic) NSString *ResidentialCity;
- (IBAction)RegisterBtn:(id)sender;

- (IBAction)GetOTP:(id)sender;
- (IBAction)TermsServices:(id)sender;

@end
