//
//  SidemenuViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 11/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUser.h"
@interface SidemenuViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *menutable;
@property (strong, nonatomic)NSString *userName;
@property (strong, nonatomic)NSString *Password;
@property (strong, nonatomic) SUser *data;
@end
