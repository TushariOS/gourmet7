//
//  SDBManager.h
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabaseAdditions.h"

@class FMDatabase;


@interface SDBManager : NSObject {
    NSString * _name;
}
@property (nonatomic, readonly) FMDatabase * dataBase;
+(SDBManager *) defaultDBManager;
- (void) close;

@end
