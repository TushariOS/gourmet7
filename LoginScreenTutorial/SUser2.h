//
//  SUser2.h
//  Gourmet7
//
//  Created by Mac Pro on 20/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SUser2 : NSObject

@property (strong, nonatomic) NSString *RestoId;
@property (strong, nonatomic) NSString *visitpurpose;
@property (strong, nonatomic) NSString *Restoname;
@property (strong, nonatomic) NSString *RequestId;
@property (nonatomic) float FRating;
@property (nonatomic) float Arating;
@property (nonatomic) float Srating;
@property (nonatomic) float Vrating;
@property (nonatomic) float Hrating;
@property (nonatomic) float Avgrating;
@property (nonatomic, strong) NSString *BillAmt;
@property (nonatomic, strong) NSString* Checkindate;
@property (nonatomic, strong) NSString *Checkintime;
@property (nonatomic, strong) NSString *checkoutdate;
@property (nonatomic, strong) NSString *checkouttime;
@property (nonatomic, strong) NSString *SavedAmt;
@property (nonatomic, strong) NSString *purpose;
@property (nonatomic, strong) NSString *finaldate;
@property (nonatomic, strong)NSString *finaltime;



@end
