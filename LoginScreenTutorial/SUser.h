//
//  SUser.h
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface SUser : NSObject

@property (nonatomic, assign) int ID;
@property (nonatomic, copy) NSString *role;
@property (nonatomic, copy) NSString *token;
@property (nonatomic, copy) NSString *firstName;
@property (nonatomic, copy) NSString *lastName;
@property (nonatomic, copy) NSString *loginId;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *cityId;
@property (nonatomic, copy) NSString *countryId;
@property (nonatomic, copy) NSString *outletId;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *todaysDiscount;
@property (nonatomic, copy) NSString *complementary;
@property (nonatomic, copy) NSString *avgRating;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *telephone1;
@property (nonatomic, copy) NSString *areaName;
@property (nonatomic,copy) UIImageView *Logoimage;
@property (nonatomic, copy) NSString *logoimagepath;
@property (nonatomic,copy) NSString *specialty;
@property (nonatomic, copy) NSString *addressLine2;
@property (nonatomic, copy) NSString *checkIns;
@property (nonatomic,copy) NSString *giftAMeal;
@property (nonatomic, copy) NSString *promocodes;
@property (nonatomic) BOOL *happyhours;
@property (nonatomic) BOOL *offers;
@property (nonatomic) BOOL opened;
@property (nonatomic, copy) NSString *mondayTimingSlabs;
@property (nonatomic, copy) NSString *tuesdayTimingSlabs;
@property (nonatomic, copy) NSString *wednesdayTimingSlabs;
@property (nonatomic, copy) NSString *thursdayTimingSlabs;
@property (nonatomic, copy) NSString *fridayTimingSlabs;
@property (nonatomic, copy) NSString *saturdayTimingSlabs;
@property (nonatomic, copy) NSString *sundayTimingSlabs;
@property (nonatomic, copy) NSString *startTime;
@property (nonatomic, copy) NSString *endTime;
@property (nonatomic, copy) NSString *outletMembershipType;
@property (nonatomic) CLLocationDegrees latitude;
@property (nonatomic) CLLocationDegrees longitude;
@property (nonatomic, copy) NSString *restaurant;
@property (nonatomic, copy) NSString *restoimages;
@property (nonatomic, copy) NSString *mobilepath;
@property (nonatomic, copy) NSString *mobilethumbpath;
@property (nonatomic, copy) NSString *originalimagepath;
@property (nonatomic) BOOL compliment;
@property (nonatomic, strong) NSString *requestId;
@property (nonatomic) BOOL Active;
@property (strong, nonatomic)NSString *phonenumber;
@property (strong, nonatomic)NSString *CityName;
@property (nonatomic, copy)NSString *Countryname;




@end