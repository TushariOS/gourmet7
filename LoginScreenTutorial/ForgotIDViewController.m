#import "ForgotIDViewController.h"
#import "SUser.h"
#import "ViewController.h"
#import "AFNetworking.h"

@interface ForgotIDViewController ()

@end

@implementation ForgotIDViewController
@synthesize EmailID;
- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

-(void)forgotIdservice
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    NSString *URLString = [NSString stringWithFormat:@"http://167.114.0.116:9090/SubmitForgotLogin"];
    NSDictionary *params = @{@"emailId": self.EmailID.text,
                             @"isRequestForLoginId":@"isRequestForLoginId",
                             @"client":@"ANDROID",
                             };
    
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         [self getresponse:responseObject];
         
     } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];

}
- (IBAction)SubmitBtn:(id)sender
{
    
    [self forgotIdservice];
    [self.navigationController popViewControllerAnimated:YES];


}



-(void)getresponse:(NSDictionary *)jsondata
{
    NSString *msg = [jsondata valueForKey:@""];
}



- (IBAction)GoBackBtn:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
