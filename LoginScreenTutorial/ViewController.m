//
//  ViewController.m
//  Gourmet7
//
//  Created by Sagar Pachorkar on 09/19/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//
#import "ViewController.h"
#import "AppDelegate.h"
#import "SUser.h"
#import "SUserDB.h"
#import "Reachability.h"
#import "RestaurantListing.h"
#import "AFNetworking.h"
@interface ViewController ()<NSURLSessionDelegate>

@end

@implementation ViewController
{
    UIView *loadingview;
}
@synthesize data,error_msg,currentcity,Loginpatchview,Registerbutton,loginpatch;
- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
       UIImage *logoimage = [UIImage imageNamed:@"logo_small.png"];
    
    self.navigationItem.titleView = [[UIImageView alloc]initWithImage:logoimage];
    
   // [titleView sizeToFit];
    Loginpatchview.layer.cornerRadius = 4;
    Registerbutton.layer.cornerRadius = 4;
    loginpatch.layer.cornerRadius = 4;
    [self loadingView];
    [loadingview setHidden:YES];
        
	Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus netStatus = [reach currentReachabilityStatus];
    [[SUserDB singleton]deleteAllRecords:data];
    if (netStatus != NotReachable)
    {
        NSLog(@"Network is Available");
        NSString *UsrId = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"USER_ID"];
        NSString *UsePwd = [[NSUserDefaults standardUserDefaults]
                                stringForKey:@"USER_PASSWORD"];
        
        if(UsrId != nil){
            NSString *txtClient = @"ANDROID";
            NSString *txtURL = @"AndroidLogin";
            NSString *txtRole = @"7";
            
        [loadingview setHidden:NO];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
        manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
        
        manager.securityPolicy.allowInvalidCertificates = YES;
        manager.requestSerializer.timeoutInterval = 10;
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
        
        NSString *URLString = [NSString stringWithFormat:@"http://167.114.0.116:9090/customLogin"];
        NSDictionary *params = @{@"j_username": UsrId,
                                 @"j_password": UsePwd,
                                 @"client":txtClient,
                                 @"url":txtURL,
                                 @"j_role":txtRole};
        
        
        [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             NSLog(@"JSON: %@", responseObject);
             
             [self catchresponse:responseObject];
         } failure:^(AFHTTPRequestOperation *operation, NSError *error)
         {
             NSLog(@"Error: %@", error);
         }];
        }
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"No connectivity" message:@"The network is not available. Please check your network connection in the settings app." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil , nil];
        [alertView show];
    }
}

-(void)loadingView
{
    loadingview = [[UIView alloc]initWithFrame:CGRectMake(100, 400, 80, 80)];
    CGRect frame = loadingview.frame;
    frame.origin.x = self.view.frame.size.width / 2 - frame.size.width / 2;
    frame.origin.y = self.view.frame.size.height / 2 - frame.size.height / 2;
    loadingview.frame = frame;
    loadingview.backgroundColor = [UIColor clearColor];
    
    loadingview.layer.cornerRadius = 5;
    
    UIActivityIndicatorView *activityView=[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityView.center = CGPointMake(loadingview.frame.size.width / 2.0, 35);
    [activityView startAnimating];
    activityView.tag = 100;
    [loadingview addSubview:activityView];
    [self.view addSubview:loadingview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}



- (IBAction)signinClicked:(id)sender
{
    NSInteger success = 0;
    
    @try
    {
        if([[self.txtUsername text] isEqualToString:@""] && [[self.txtPassword text] isEqualToString:@""] )
        {
            [self alertStatus:@"Please enter Login ID and Password!" :@"LogIn Failed!" :0];
            
        }
        
        else if ([[self.txtUsername text]isEqualToString:@""])
        {
            [self alertStatus:@"Please enter Login ID!" :@"Login Failed" :0];
        }
       else if ([[self.txtPassword text]isEqualToString:@""])
        {
            [self alertStatus:@"Please enter Password!":@"Login Failed":0];
        }
        else
        {
            
            NSString *txtClient = @"ANDROID";
            NSString *txtURL = @"AndroidLogin";
            NSString *txtRole = @"7";

            
            [loadingview setHidden:NO];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
           
            manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
           
            manager.securityPolicy.allowInvalidCertificates = YES;
            manager.requestSerializer.timeoutInterval = 10;
           manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
            
            NSString *URLString = [NSString stringWithFormat:@"http://167.114.0.116:9090/customLogin"];
            NSDictionary *params = @{@"j_username": self.txtUsername.text,
                                                                         @"j_password": self.txtPassword.text,
                                                                         @"client":txtClient,
                                                                         @"url":txtURL,
                                                                         @"j_role":txtRole};
            
            
            [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
            {
                NSLog(@"JSON: %@", responseObject);
                
                [self catchresponse:responseObject];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error)
            {
                [loadingview setHidden:YES];
                NSLog(@"Error: %@", error);
            }];
             }
    }
    @catch (NSException * e)
    {
        NSLog(@"Exception: %@", e);
        [self alertStatus:@"Please enter correct Login ID and Password!" :@"Error!" :0];
    }
}


-(void)catchresponse:(NSDictionary *)jsonData
{
    
    
    
    
   
    
    //NSString *token = [jsonData valueForKey:@"token"];
    data = [SUser new];
    data.token=[jsonData objectForKey:@"token"];
    data.role = [jsonData valueForKey:@"role"] ;
    data.firstName = [jsonData valueForKey:@"firstName"];
    data.lastName = [jsonData valueForKey: @"lastName"];
    data.loginId = [jsonData valueForKey:@"loginId"];
    data.userId = [jsonData valueForKey:@"userId"];
    data.cityId = [jsonData valueForKey:@"cityId"];
    data.countryId = [jsonData valueForKey:@"countryId"];
    data.CityName = [jsonData valueForKey:@"cityName"];
    data.Countryname = [jsonData valueForKey:@"countryName"];

    NSString *cityname = [jsonData valueForKey:@"cityName"];
    NSString *countryname = [jsonData valueForKey:@"countryName"];
    //data.CityName = [NSString stringWithFormat:@"%@,%@",cityname,countryname];
     currentcity = [NSString stringWithFormat:@"%@,%@",cityname,countryname];
    
    NSString *ErrorMsg = [jsonData valueForKey:@"errorCode"];
    
    if([ErrorMsg isEqualToString:@"Invalid login credentials. Try again!"])
    {
    [loadingview setHidden:YES];
    }
    else
    {
         NSString *UserID = data.loginId;
        [[NSUserDefaults standardUserDefaults] setObject:UserID forKey:@"USER_ID"];
        [[NSUserDefaults standardUserDefaults] synchronize];//tushar , storing username password in nsuserdefalut for auto login
        NSString *Userpassword = _txtPassword.text;
        [[NSUserDefaults standardUserDefaults] setObject:Userpassword forKey:@"USER_PASSWORD"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        [[SUserDB singleton]saveUserDetails:data];
        NSLog(@"%@",[NSString stringWithFormat:@
                     "%@",[[SUserDB singleton]fetchcityname]]);
        [loadingview setHidden:YES];
    }
    
    NSLog(@"JsonData: %@",jsonData);
    NSLog(@"Token: %@",data.token);
    
    
    
    if([[self.txtUsername text]  isEqual:data.loginId])
    {
        
        DashboardViewController *dash = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
       // dash.mycity = currentcity;
        [self.navigationController pushViewController:dash animated:YES];
        NSLog(@"Login SUCCESS");
    }
    
    else
    {
        [self alertStatus:@"Please enter correct Login ID and Password!":@"LogIn Failed":0];
    }

}

- (void) alertStatus:(NSString *)msg :(NSString *)title :(int) tag
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil, nil];
    alertView.tag = tag;
    [alertView show];
}
- (IBAction)backgroundTap:(id)sender
{
    [self.view endEditing:YES];
}

- (IBAction)Registerbtn:(id)sender
{
  
    RegisterViewController *regi = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController: regi animated: YES];

}
-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
