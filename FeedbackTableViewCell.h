//
//  FeedbackTableViewCell.h
//  Gourmet7
//
//  Created by Mac Pro on 17/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *dateandtime;
@property (weak, nonatomic) IBOutlet UITextView *feedback;

@end
