//
//  DropdownlistViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 28/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "DropdownlistViewController.h"
#import "customcell.h"
#import "DashboardViewController.h"
#import "SUserDB.h"
@interface DropdownlistViewController ()

@end

@implementation DropdownlistViewController
{
    NSMutableArray *ArraofNames;
    NSMutableArray *searchResults;
    NSMutableArray *ArrayofCities;
    NSMutableArray *ArrayofCityIds;

}
@synthesize droplisttable,MyArray,CityName,Data,aUser,searchbar,isFiltered,Currentcity,SearchStrUser,SearchResultCity,SearchResultCityId,Citydictionary;
- (void)viewDidLoad
{
    
    
    [super viewDidLoad];
    NSArray *keys = [NSArray arrayWithObjects:@"Agra", @"Ahmedabad",@"Allahabad", @"Amritsar",@"Bengaluru",@"Bhopal", @"Bikaner",@"Chandigarh", @"Chennai",@"Coimbatore", @"Dehradun",@"Delhi NCR", @"Gwalior",@"Hyderabad", @"Indore",@"Jabalpur", @"Jaipur",@"Jaisalmer", @"Jodhpur",@"Kanpur", @"Kochi",@"Kolkata", @"Lucknow",@"Ludhiana", @"Mangalore",@"Mumbai", @"Mysore",@"Nagpur",@"Nashik",@"Patna",@"Puducherry",@"Pune",@"Raipur",@"Ranchi",@"Secunderabad",@"Udaipur", nil];
    
    NSArray *objects = [NSArray arrayWithObjects:@"31", @"9",@"41",@"16",@"5", @"8",@"264", @"268",@"13", @"273",@"229", @"4",@"275", @"10",@"3", @"12",@"14", @"270",@"269", @"266",@"274", @"15",@"272", @"196",@"265", @"1",@"17", @"6",@"7",@"18",@"184",@"2",@"267",@"271",@"11",@"263", nil];
    Citydictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];

    
    
    
    if (!ArrayofCities)
        ArrayofCities =[[NSMutableArray alloc] init];
    
    if (!ArrayofCityIds)
        ArrayofCityIds =[[NSMutableArray alloc] init];
    for (id jsonData in MyArray)
    {
        
        Data = [SUser new];
        Data.CityName = [jsonData valueForKey:@"cityName"];
        Data.cityId = [jsonData valueForKey:@"cityId"];
        NSString *str = [jsonData valueForKey:@"cityName"];
        [ArrayofCities addObject:str];
        NSLog(@"%@",Data.CityName);
        NSLog(@"%@",Data.cityId);
        NSString *str1 = [jsonData valueForKey:@"cityId"];
        [ArrayofCityIds addObject:str1];

        if (!ArraofNames)
            ArraofNames =[[NSMutableArray alloc] init];
        [ArraofNames addObject:Data];
    }
    [[self droplisttable]setDelegate:self];
    [[self droplisttable]setDataSource:self];
    [[self searchbar]setDelegate:self];
   // [self.droplisttable registerNib:[UINib nibWithNibName:@"customcell" bundle:nil] forCellReuseIdentifier:@"cell"];
    NSLog(@"%@",ArraofNames);
    
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isFiltered == YES)
    {
        return [searchResults count];
        
    }
    
    else
    {
        return [ArraofNames count];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellidentifier = @"cell";
    
    customcell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier];
    if (cell == nil)
    {
        cell = [[customcell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
    }
    aUser = [ArraofNames objectAtIndex:indexPath.row];
    
    if (isFiltered == YES)
    {
        SearchStrUser  = [searchResults objectAtIndex:indexPath.row];
        cell.Namelabe.text = SearchStrUser;
        
    }
    else
    {
        cell.Namelabe.text = aUser.CityName;
    }
    
    return cell;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text.length == 0)
    {
        isFiltered = NO;
         [searchResults removeAllObjects];
    }
    else
    {
        //Remove all objects first.
        [searchResults removeAllObjects];
        [SearchResultCityId removeAllObjects];
        isFiltered = YES;
        searchResults = [[NSMutableArray alloc]init];
        SearchResultCityId = [[NSMutableArray alloc]init];
        
        NSLog(@"%@",ArrayofCities);
     //   NSString *searchString = searchBar.text;
        
        for (NSString *tempStr in ArrayofCities) {
                NSRange citynamerange = [tempStr rangeOfString:searchText options:NSCaseInsensitiveSearch];
                
                if (citynamerange.location != NSNotFound)
                {
                    [searchResults addObject:tempStr];
                    [SearchResultCityId addObject:tempStr];
                    
                    
                }
            }
         [droplisttable reloadData];
        }

    
    
   

}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
       aUser = [ArraofNames objectAtIndex:indexPath.row];
    
    Currentcity = [ArraofNames objectAtIndex:indexPath.row];
 
    DashboardViewController *dash = [self.storyboard instantiateViewControllerWithIdentifier:@"dashboard"];
   
   
    
   
    if (isFiltered ==YES)
                {
                    [[SUserDB singleton]updateUserCityName:[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexPath.row]]];
                    [[SUserDB singleton]updateUserCityId:[NSString stringWithFormat:@"%@",[Citydictionary valueForKey:[NSString stringWithFormat:@"%@",[searchResults objectAtIndex:indexPath.row]]]]];
                    NSLog(@"%@",[NSString stringWithFormat:@"%@",[SearchResultCityId objectAtIndex:indexPath.row]]);
                    NSLog(@"%@",[NSString stringWithFormat:@"%@",[[SUserDB singleton]fetchcityId]]);
                }
                else
                {
                    NSLog(@"%@",[NSString stringWithFormat:@"%@",aUser.CityName]);
                    [[SUserDB singleton]updateUserCityName:[NSString stringWithFormat:@"%@",aUser.CityName]];
                    [[SUserDB singleton]updateUserCityId:[NSString stringWithFormat:@"%@",aUser.cityId]];
                    NSLog(@"%@",[NSString stringWithFormat:@"%@",aUser.cityId]);
                    NSLog(@"%@",[NSString stringWithFormat:@"%@",[[SUserDB singleton]fetchcityId]]);
                    

                }

    
    [self.navigationController pushViewController:dash animated:YES];
    NSLog(@"Login SUCCESS");
    }


//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
//{
//    if ([segue.identifier isEqualToString:@"showdashboard"])
//    {
//        NSIndexPath *indexpath = [self.droplisttable indexPathForSelectedRow];
//        
//        DashboardViewController *destVCobj = segue.destinationViewController;
//        
//        destVCobj.mycity=[ArraofNames objectAtIndex:indexpath.row];
//        NSLog(@"%@",ArraofNames);
//        NSLog(@"%i",indexpath.row);
//        if (isFiltered ==YES)
//        {
//            destVCobj.mycity = [searchResults objectAtIndex:indexpath.row];
//        }
//        else
//        {
//            destVCobj.mycity = [ArraofNames objectAtIndex:indexpath.row];
//            NSLog(@"%@",[ArraofNames objectAtIndex:indexpath.row]);
//        }
//    }
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

@end
