//
//  FilterViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 23/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import "FilterViewController.h"
#import "RestaurantListing.h"
#import "FilterCollectionViewCell.h"
#import "PremiumCollectionViewCell.h"
#import "FacilitiesCollectionViewCell.h"
#import "HeaderClass.h"


@interface FilterViewController ()
{
    BOOL shareEnabled;
    NSMutableArray *selectedArray;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scroller;
@property (weak, nonatomic) IBOutlet UIView *contentview;
@property (weak, nonatomic) IBOutlet UICollectionView *CategoryCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *PremiumFeatureCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *FacilitiesCollectionView;




@end
@implementation FilterViewController
@synthesize FilterArray,PremiumArray,FacilityArray,RestoNameText,CuisineText,AreaText,FinalArray,Sectionheader,delegate,sidebarbtn;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    
    }
    return self;
}
-(void)viewDidLoad
{
    
        self.revealViewController.delegate = self;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarbtn setTarget: self.revealViewController];
        [self.sidebarbtn setAction: @selector( mysidebarAnimated:)];
        
    }
  // [self.CategoryCollectionView registerClass:[FilterCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
  //  [self.PremiumFeatureCollectionView registerClass:[PremiumCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
 //   [self.FacilitiesCollectionView registerClass:[FacilitiesCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    self.CategoryCollectionView.dataSource = self;
    self.CategoryCollectionView.delegate = self;
    self.PremiumFeatureCollectionView.dataSource = self;
    self.PremiumFeatureCollectionView.delegate = self;
    self.FacilitiesCollectionView.dataSource = self;
    self.FacilitiesCollectionView.delegate = self;
    
    [self.CategoryCollectionView reloadData];

    [self.FacilitiesCollectionView reloadData];

    [self.PremiumFeatureCollectionView reloadData];

    selectedArray = [NSMutableArray array];
    FilterArray = [[NSMutableArray alloc]initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:@"Sun-Day-Normal.png",@"images",@"open",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc] initWithObjectsAndKeys:@"Offer-Normal.png",@"images",@"Offers",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Promocodes-Normal.png",@"images",@"Promocodes",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Discount-Normal.png",@"images",@"Discount",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Complimentary-Normal.png",@"images",@"Complimentary",@"titles",@"false",@"selected", nil],
                   [[NSDictionary alloc]initWithObjectsAndKeys:@"Followers-Normal02.png",@"images",@"Following",@"titles",@"false",@"selected", nil],nil];
   
    
        PremiumArray = [[NSMutableArray alloc]initWithObjects:[[NSDictionary alloc] initWithObjectsAndKeys:@"Self-Service-Normal.png",@"images",@"Call for service",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Table-reserved-Normal.png",@"images",@"Table Booking",@"titles",@"false",@"selected", nil],
                     [[NSDictionary alloc]initWithObjectsAndKeys:@"Happy-H-Normal.png",@"images",@"Happy Hours",@"titles",@"false",@"selected", nil],nil];
    
    
    
    
   
    
    FacilityArray = [[NSMutableArray alloc]initWithObjects:[[NSDictionary alloc]initWithObjectsAndKeys:@"AC-Normal.png",@"images",@"AC",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"CCard-Normal.png",@"images",@"CreditCard",@"titles",@"false",@"selected", nil],
                       [[NSDictionary alloc]initWithObjectsAndKeys:@"Wifi-Normal.png",@"images",@"Wi-Fi",@"titles",@"false",@"selected", nil],
                      [[ NSDictionary alloc]initWithObjectsAndKeys:@"non_veg-Normal.png",@"images",@"Non-Vegetarian",@"titles",@"false",@"selected",nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Parking-Normal.png",@"images",@"Parking",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Pet-Friendly-Normal.png",@"images",@"Pet Friendly",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Toilet-Normal.png",@"images",@"Toilet",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Servers-Alcohol-Normal.png",@"images",@"Serves Alcohol",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Smoking-Normal.png",@"images",@"Smoking",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Waiters-Normal.png",@"images",@"Eng. Speaking Waiters",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Privet-party-Normal.png",@"images",@"Pvt. Party Area",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Disable-Fraindly-Normal.png",@"images",@"Disabled Friendly",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Live-food-counter-Normal.png",@"images",@"Live Food counter",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Take-away-Normal.png",@"images",@"Take Away",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Snacks-Normal.png",@"images",@"Snacks",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Dine-in-Normal.png",@"images",@"Dine in",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"outdoor-seating-Normal.png",@"images",@"Outdoor Seating",@"titles",@"false",@"selected",nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Table-reserved-Normal.png",@"images",@"Table Reservation",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Bar-Area-Normal.png",@"images",@"Bar Area",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Plays-Music-Normal.png",@"images",@"Play Music",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Home-Delivery-Normal.png",@"images",@"Home Delivery",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Corporate-Normal.png",@"images",@"Corporate",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Live-Sports-Normal.png",@"images",@"Live Sports",@"titles",@"false",@"selected",nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Banquet-Normal.png",@"images",@"Banquet",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Dress-Code-Normal.png",@"images",@"Dress Code",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"24x7-Normal.png",@"images",@"Open 24x7",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Sunday-Bunch-Normal.png",@"images",@"Sunday Brunch",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Self-Service-Normal.png",@"images",@"Self Service",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Jain-Food-Normal.png",@"images",@"Jain Food",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Buffet-Lunch-Normal.png",@"images",@"Buffet Lunch",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Buffet-Dinner-Normal.png",@"images",@"Buffet Dinner",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Coupan-accepted-Normal.png",@"images",@"Coupons Accepted",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Moon-Night-Normal.png",@"images",@"Midnight Buffet",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Party-Orders-Normal.png",@"images",@"Party Orders",@"titles",@"false",@"selected",nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"Theme-Normal.png",@"images",@"Theme",@"titles",@"false",@"selected", nil],
                      [[NSDictionary alloc]initWithObjectsAndKeys:@"In-Mall-Normal.png",@"images",@"In-Mall",@"titles",@"false",@"selected", nil],nil];
    Sectionheader = [[NSMutableArray alloc]initWithObjects:@"CATEGORY",@"PREMIUM FEATURE",@"FACILITIES",nil];
    
    FinalArray = [NSMutableArray arrayWithObjects:FilterArray,PremiumArray,FacilityArray, nil];
    
     RestoNameText.layer.borderColor=[[UIColor orangeColor]CGColor];
     CuisineText.layer.borderColor=[[UIColor orangeColor]CGColor];
     AreaText.layer.borderColor=[[UIColor orangeColor]CGColor];
    
   
    [super viewDidLoad];
}

//-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
//{
//    HeaderClass * header = [mycollectionview dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"myheader" forIndexPath:indexPath];
//    header.myheader.text = [Sectionheader objectAtIndex:indexPath.section];
//    
//    return header;
//}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
 
    if (collectionView == self.CategoryCollectionView) {
        
   
        [_CategoryCollectionView.collectionViewLayout invalidateLayout];
        return  1;
    }
    else if (collectionView == self.PremiumFeatureCollectionView) {
    
    
    
    [_PremiumFeatureCollectionView.collectionViewLayout invalidateLayout];
        return  1;
    
    }
    
    else{
        
    //means it is facilities collection view
        [_FacilitiesCollectionView.collectionViewLayout invalidateLayout];
        return  1;

    }
    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.CategoryCollectionView) {
        //return [[FinalArray objectAtIndex:section] count];
        return FilterArray.count;
    }
    else if (collectionView == self.PremiumFeatureCollectionView) {
        
        //return [[FinalArray objectAtIndex:section] count];
        //return [[PremiumArray objectAtIndex:section] count];
        return PremiumArray.count;
        
    }
    
    else{
        //return [[FinalArray objectAtIndex:section] count];
        //means it is facilities collection view
         //return [[FacilityArray objectAtIndex:section] count];
        return FacilityArray.count;
    }
   
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    if (collectionView == self.CategoryCollectionView) {
        FilterCollectionViewCell *cell = [_CategoryCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.Facilityimages.image = [UIImage imageNamed:[[FilterArray objectAtIndex:indexPath.row] valueForKey:@"images"]];
        cell.Facilityname.text = [[FilterArray objectAtIndex:indexPath.item]valueForKey:@"titles"];
        //cell.selected = YES;
       
        [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        [cell.layer setBorderWidth:2.0f];
        [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"border.png"]];
       return cell;
    }
    else if (collectionView == self.PremiumFeatureCollectionView) {
        PremiumCollectionViewCell *cell = [_PremiumFeatureCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.Facilityimages.image = [UIImage imageNamed:[[PremiumArray objectAtIndex:indexPath.row] valueForKey:@"images"]];
        cell.Facilityname.text = [[PremiumArray objectAtIndex:indexPath.item]valueForKey:@"titles"];
        //cell.selected = YES;
        
        
        [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        [cell.layer setBorderWidth:2.0f];
        [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"border.png"]];
        return cell;
        
       
        
        
    }
    
    else{
        //means it is facilities collection view
        
        FacilitiesCollectionViewCell *cell = [_FacilitiesCollectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
        cell.Facilityimages.image = [UIImage imageNamed:[[FacilityArray objectAtIndex:indexPath.row] valueForKey:@"images"]];
        cell.Facilityname.text = [[FacilityArray objectAtIndex:indexPath.item]valueForKey:@"titles"];
        //cell.selected = YES;
        
        
        [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
        [cell.layer setBorderWidth:2.0f];
        [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"border.png"]];
        return cell;
        
        
    }
    
    

    
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == self.CategoryCollectionView) {
        
        NSLog(@"selected cell is %ld",(long)indexPath.item);
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[FilterArray objectAtIndex:indexPath.item];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"true" forKey:@"selected"];
        [FilterArray replaceObjectAtIndex:indexPath.item withObject:newDict];
        FilterCollectionViewCell *cell = (FilterCollectionViewCell *) [self.CategoryCollectionView cellForItemAtIndexPath:indexPath];
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checked.png"]];
    }
    else if (collectionView == self.PremiumFeatureCollectionView) {
        
        
        NSLog(@"selected cell is %ld",(long)indexPath.item);
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[PremiumArray objectAtIndex:indexPath.item];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"true" forKey:@"selected"];
        [PremiumArray replaceObjectAtIndex:indexPath.item withObject:newDict];
        FilterCollectionViewCell *cell = (FilterCollectionViewCell *) [self.PremiumFeatureCollectionView cellForItemAtIndexPath:indexPath];
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checked.png"]];
        
        
    }
    
    else{
        //means it is facilities collection view
        
        
        NSLog(@"selected cell is %ld",(long)indexPath.item);
        
        NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
        NSDictionary *oldDict = (NSDictionary *)[FacilityArray objectAtIndex:indexPath.item];
        
        [newDict addEntriesFromDictionary:oldDict];
        [newDict setObject:@"true" forKey:@"selected"];
        [FacilityArray replaceObjectAtIndex:indexPath.item withObject:newDict];
        FilterCollectionViewCell *cell = (FilterCollectionViewCell *) [self.FacilitiesCollectionView cellForItemAtIndexPath:indexPath];
        
        cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checked.png"]];
    }
    


}
-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{

    FilterCollectionViewCell *cell = (FilterCollectionViewCell *) [self.CategoryCollectionView cellForItemAtIndexPath:indexPath];
    
    cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"border.png"]];



}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    [self.scroller layoutIfNeeded];
    self.scroller.contentSize = self.contentview.bounds.size;
    self.scroller.contentSize = CGSizeMake(320.0f,4270.0f);
}

-(void)dismissKeyboard
{
    [self.view endEditing:YES];
    
//    [RestoNameText resignFirstResponder];
//    [CuisineText resignFirstResponder];
//    [AreaText resignFirstResponder];
}

#pragma mark -collectionview Methods
- (void)reloadItemsAtIndexPaths:(NSArray *)indexPaths
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)Backbutton:(id)sender
{
    RestaurantListing *dash = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantListing"];
    
    [self.navigationController pushViewController:dash animated:YES];
}

- (IBAction)Searchbtn:(id)sender
{
    NSString *RestaurantNme = RestoNameText.text;
    NSString *cuisine = CuisineText.text;
    NSString *Area = AreaText.text;
    NSMutableArray *SearchArray = [[NSMutableArray alloc]initWithObjects:RestaurantNme,cuisine,Area, nil];
    NSMutableArray *StrArray = [NSMutableArray arrayWithObjects:RestaurantNme,cuisine,Area, nil];
    [self.delegate PassId:self PassedId:StrArray];
    [self.delegate PassId:self PassedId:SearchArray];
    [self.delegate PassvisitPurpose:self PassvisitPurpose:FinalArray];
    //RestaurantListing *Listing = [[RestaurantListing alloc]init];
   // Listing.MyFilterArray = FinalArray;
    
    RestaurantListing *dash = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantListing"];
    dash.FilterArray = FinalArray;
    
    [self.navigationController pushViewController:dash animated:YES];
}
@end
