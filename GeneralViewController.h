//
//  GeneralViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 02/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "detailViewController.h"

@interface GeneralViewController : UIViewController

- (IBAction)button:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *Mybutton;
@property (weak, nonatomic)NSDictionary *MyDict;
@property(strong, nonatomic)NSArray *myArray;
@property (strong, nonatomic)NSDictionary *Dict;
@property (strong, nonatomic) NSString *offer;
@property (strong, nonatomic)NSString *offertitle;
@property (strong, nonatomic)NSString *groupdetails;
@property (strong, nonatomic)NSString *TandC;
@end
