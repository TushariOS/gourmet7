//
//  CheckoutRating.m
//  Gourmet7
//
//  Created by Mac Pro on 23/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "CheckoutRating.h"
#import "SUserDB.h"
#import "SUser2.h"
#import "AFNetworking.h"

@interface CheckoutRating ()

@end

@implementation CheckoutRating
@synthesize rates5,rates4,rates3,RateDict,rates1,rates2,requestId,Foodrating,Ambience,Servicerating,valueformonyrting,Hygienerting,YouRated,Vpurpose,msg,message,messagetext,token,data2,userId,outletId1,visitpurpose;

- (void)viewDidLoad
{
    [super viewDidLoad];
    token = [[SUserDB singleton]fetchTokenNumber];
    userId = [[SUserDB singleton]fetchuserId];
    
    NSString *Billamount = [[RateDict valueForKey:@"billAmount"]stringValue];
    NSDictionary *restoDict = [RateDict objectForKey:@"outlet"];
    
    requestId = [RateDict valueForKey:@"requestId"];
    
    outletId1 = [restoDict valueForKey:@"outletId"];
    
    NSString *Points = [[RateDict valueForKey:@"pointEarned"] stringValue];
    NSString *visitP = [RateDict valueForKey:@"visitPurpose"];
    
    if ([Billamount isEqualToString:@"0"])
    {
        msg = @"The restaurant has yet not entered your bill. The points will be credited once the restaurant enters the bill amount.";
    }
    else
    {
        msg = [NSString stringWithFormat:@"Your bill amount is %@ and you earned %@ points",Billamount,Points];
    }
    message = msg;
    
    messagetext.text = message;
    
    if ([visitP isEqualToString:@"BUSINESS"])
    {
        Vpurpose = @"1";
    }
    else if ([visitP isEqualToString:@"FAMILY"])
    {
        Vpurpose = @"2";
    }
    else if ([visitP isEqualToString:@"FRIENDS"])

    {
        Vpurpose = @"3";
    }
    else if ([visitP isEqualToString:@"ROMANTIC"])
    {
        Vpurpose = @"4";
    }
    else if ([visitP isEqualToString:@"SOLO"])
    {
        Vpurpose = @"5";
    }
    
    visitpurpose = Vpurpose;
    
    [self YouratedUrl];
    
    

}

-(void)Displayratings
{
    
    self.Foodrating.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Foodrating.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Foodrating.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Foodrating.rating = data2.FRating;
    self.Foodrating.editable = YES;
    self.Foodrating.maxRating = 5;
    self.Foodrating.delegate = self;
    
    self.Ambience.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Ambience.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Ambience.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Ambience.rating1 = data2.Arating;
    self.Ambience.editable = YES;
    self.Ambience.maxRating = 5;
    self.Ambience.delegate = self;
    
    self.Servicerating.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Servicerating.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Servicerating.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Servicerating.rating2 = data2.Srating;
    self.Servicerating.editable = YES;
    self.Servicerating.maxRating = 5;
    self.Servicerating.delegate = self;
    
    self.valueformonyrting.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.valueformonyrting.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.valueformonyrting.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.valueformonyrting.rating3 = data2.Vrating;
    self.valueformonyrting.editable = YES;
    self.valueformonyrting.maxRating = 5;
    self.valueformonyrting.delegate = self;
    
    self.Hygienerting.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Hygienerting.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Hygienerting.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Hygienerting.rating4 = data2.Hrating;
    self.Hygienerting.editable = YES;
    self.Hygienerting.maxRating = 5;
    self.Hygienerting.delegate = self;
    
    self.YouRated.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.YouRated.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.YouRated.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.YouRated.rating4 = data2.Avgrating;
    self.YouRated.editable = NO;
    self.YouRated.maxRating = 5;
    self.YouRated.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(void)RatingUrl
{
     NSString *url = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=/DeviceServerPush/SaveVisitPurpose/&memberId=%@&outletId=%@&requestId=%@&visitPurpose=%@&rating1=%f&rating2=%f&rating3=%f&rating4=%f&rating5=%f&token=%@",userId,outletId1,requestId,visitpurpose,rates1,rates2,rates3,rates4,rates5,token];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self Parse:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
   
}
-(void)Parse:(NSMutableDictionary *)response1
{
    NSLog(@"response is %@",response1);
}


-(void)YouratedUrl
{
    
     NSString *url = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=/Restaurant/RatedRestaurantValue/%@/%@&token=%@",userId,outletId1,token];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        [self Parse1:responseObject];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
    
}
-(void)Parse1:(NSMutableDictionary *)response
{
    data2 = [SUser2 new];
    data2.FRating = [[response valueForKey:@"rating1"]floatValue];
    data2.Arating = [[response valueForKey:@"rating2"]floatValue];
    data2.Srating = [[response valueForKey:@"rating3"]floatValue];
    data2.Vrating = [[response valueForKey:@"rating4"]floatValue];
    data2.Hrating = [[response valueForKey:@"rating5"]floatValue];
    data2.Avgrating = [[response valueForKey:@"avgrating"]floatValue];
    
    [self Displayratings];
    
}


- (IBAction)Ratebutton:(id)sender
{
    [self RatingUrl];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (IBAction)Cancelbutton:(id)sender
{
    //UINavigationController *nav = [[UINavigationController alloc]init];
    [self.navigationController popViewControllerAnimated:YES];

}

-(void)ratesView:(RatesView *)RatesView ratingDidChange:(float)rating;
{
    rates1 = rating;
}
-(void)Ambiencerating:(Ambiencerating *)Ambiencerating ratingDidChange:(float)rating1;
{
    rates2 = rating1;
}

-(void)Servicerating:(Servicerating *)Servicerating ratingDidChange:(float)rating2
{
    rates3 = rating2;
}
-(void)ValueforMoney:(ValueforMoney *)ValueforMoney ratingDidChange:(float)rating3
{
    rates4 = rating3;
}
-(void)Hygienerating:(Hygienerating *)Hygienerating ratingDidChange:(float)rating4
{
    rates5 = rating4;
}
@end
