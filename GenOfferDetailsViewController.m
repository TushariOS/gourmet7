//
//  GenOfferDetailsViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 02/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "GenOfferDetailsViewController.h"

@interface GenOfferDetailsViewController ()

@end

@implementation GenOfferDetailsViewController
@synthesize Myview,offerlbl,grouplbl,conditionlbl,offer,offerArray,offerdesc,offertxtview,offerDict,groupdetails,terms;

- (void)viewDidLoad {
    [super viewDidLoad];
    
   
    [UIView animateWithDuration:0.5 animations:^{
        Myview.frame = CGRectMake(0, 500, 320, 30);
    }];

    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
    if (!offerArray || !offerArray.count)
    {
        
    }
    else{
    offerDict = [offerArray objectAtIndex:0];
    offer = [offerDict valueForKey:@"title"];
    offerdesc = [offerDict valueForKey:@"description"];
    groupdetails = [offerDict objectForKey:@"groupDetails"];
    terms = [offerDict valueForKey:@"termsandcondition"];
    
    offerlbl.text = [NSString stringWithFormat:@"%@",offer];
    offertxtview.text = [NSString stringWithFormat:@"%@",offerdesc];
    conditionlbl.text = [NSString stringWithFormat:@"%@",terms];
    }
    for (NSString *groups in groupdetails)
    {
        grouplbl.text = [NSString stringWithFormat:@"%@",groups];
    }
    
    }
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
   // CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    [UIView animateWithDuration:0.5 animations:^{
        Myview.frame = CGRectMake(0, 500, 320, 30);
    }];
    
    grouplbl.hidden = YES;
    conditionlbl.hidden = YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@" array is :%@",offerArray);
    if (!offerArray || !offerArray.count)
    {
        
    }
    else{
        offerDict = [offerArray objectAtIndex:0];
        offer = [offerDict valueForKey:@"title"];
        offerdesc = [offerDict valueForKey:@"description"];
        groupdetails = [offerDict objectForKey:@"groupDetails"];
        terms = [offerDict valueForKey:@"termsandcondition"];
        
        offerlbl.text = [NSString stringWithFormat:@"%@",offer];
        offertxtview.text = [NSString stringWithFormat:@"%@",offerdesc];
        conditionlbl.text = [NSString stringWithFormat:@"%@",terms];
    }
    for (NSString *groups in groupdetails)
    {
        grouplbl.text = [NSString stringWithFormat:@"%@",groups];
    }
    
}




- (IBAction)Groupsbtn:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        Myview.frame = CGRectMake(0, 300, 320, 265);
    }];
    
    grouplbl.hidden = NO;
    conditionlbl.hidden = YES;
}
- (IBAction)BackBtnClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)Tandconds:(id)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        Myview.frame = CGRectMake(0, 300, 320, 265);
    }];
    grouplbl.hidden = YES;
    conditionlbl.hidden = NO;
}
@end
