//
//  AboutViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 12/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController
@synthesize aboutimageview,buttonsview,myTextView,termsofservice,Privacypolicy,sidebarbtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.revealViewController.delegate = self;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarbtn setTarget: self.revealViewController];
        [self.sidebarbtn setAction: @selector( mysidebarAnimated:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    CGRect someRect = CGRectMake(0.0, 300.0, 320.0, 220.0);
    myTextView = [[UITextView alloc] initWithFrame:someRect];
    myTextView.hidden = YES;
    myTextView.editable = NO;
    myTextView.selectable = NO;
    myTextView.backgroundColor = [UIColor whiteColor];
    [self.aboutimageview addSubview:myTextView];
    termsofservice = @"Hi Sandip, how are you";
   Privacypolicy = @"Hi Sandy";
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    [UIView animateWithDuration:0 animations:^{
        buttonsview.frame = CGRectMake(0, 533, 320, 35);
    }];
    myTextView.hidden = YES;
}

- (IBAction)termsofuse:(id)sender
{
    [UIView animateWithDuration:0 animations:^{
        buttonsview.frame = CGRectMake(0, 300, 320, 35);
    }];
    myTextView.hidden = NO;
    myTextView.text = termsofservice;
}

- (IBAction)Privacypolicy:(id)sender
{
    [UIView animateWithDuration:0 animations:^{
        buttonsview.frame = CGRectMake(0, 300, 320, 35);
    }];
    myTextView.hidden = NO;
    myTextView.text = Privacypolicy;
}
@end
