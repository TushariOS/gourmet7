//
//  MyspaceViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 03/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUser2.h"

@interface MyspaceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *Checkintable;
@property (weak, nonatomic) IBOutlet UIScrollView *spacescroll;
@property (weak, nonatomic) IBOutlet UIView *Contentview;

@property(strong, nonatomic) NSDictionary *MyDict;
@property (strong, nonatomic) NSArray *MyArray;
@property (strong, nonatomic) NSMutableArray *ListArray;
@property (strong, nonatomic)SUser2 *auser;
@property (strong, nonatomic)NSNotification *mynotification;

@end
