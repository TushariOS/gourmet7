//
//  RestaurantListing.h
//  Gourmet7
//
//  Created by Sagar Pachorkar on 21/09/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "RateView.h"
#import "SUser.h"
#import <CoreLocation/CoreLocation.h>
#import "FilterViewController.h"
#import "SWRevealViewController.h"

@interface NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end

@interface RestaurantListing : UIViewController<UITableViewDelegate,UITableViewDataSource,RateViewDelegate,NSURLConnectionDelegate,CLLocationManagerDelegate,FilterViewControllerDelegate,SWRevealViewControllerDelegate,UINavigationControllerDelegate>
{
    bool open,disc,compli,follow,folloresto,moreitemsavailable,onfirstvisit,afterfollow;
}
@property (nonatomic)BOOL shouldreload;
@property (weak, nonatomic) IBOutlet UITableView *restotable;
@property (nonatomic) BOOL test;
@property (nonatomic) long start;
@property (nonatomic, strong) NSString *token;
@property (nonatomic,strong) IBOutlet UIButton *openbtn;
@property (nonatomic, strong) IBOutlet UIButton *complibtn;
@property (nonatomic, strong) IBOutlet UIButton *followbtn;
@property (nonatomic, strong) IBOutlet UIButton *discbtn;
@property (strong, nonatomic)NSString *userId;
@property (nonatomic) BOOL follow1;
@property (nonatomic) BOOL Active1;
@property (strong, nonatomic)UIButton *button;
@property (strong, nonatomic) NSString  *Listurl;
@property ( nonatomic)float currentlat;
@property (nonatomic)float currentlon;
@property (nonatomic,strong)NSMutableArray *serviceResponse1;
@property (nonatomic, strong) SUser *auser;
@property (nonatomic, strong) NSString *currenttime;
@property (nonatomic, strong) NSString *opentime;
@property (nonatomic, strong) SUser *followresto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityindicator;
@property (strong, nonatomic) NSString *theLocation;
@property (nonatomic)CLLocationDistance DistInKilometers;
@property (nonatomic,retain) CLLocationManager *locationManager;

@property (nonatomic, strong)SUser *data;
@property (strong, nonatomic)NSMutableArray *MyFilterArray;
@property (strong, nonatomic)NSMutableArray *Filter1;
@property (strong, nonatomic)NSMutableArray *Filter2;
@property (strong, nonatomic)NSMutableArray *Filter3;
@property (strong, nonatomic)NSString *Restoname;
@property (strong, nonatomic)NSString *Cuisine;
@property (strong, nonatomic)NSString *Area;
@property (strong, nonatomic)NSArray *MyArray1;
@property (strong, nonatomic)NSMutableArray *FilterArray;
@property (strong, nonatomic)NSString *RestaurantName;
@property (strong, nonatomic)NSString *CuisineName;
@property (strong, nonatomic)NSString *AreaName;
@property (strong, nonatomic)IBOutlet UIBarButtonItem *sidebarbutton;

- (IBAction)OpenNow:(id)sender;
- (IBAction)Discount:(id)sender;
- (IBAction)Complementary:(id)sender;
- (IBAction)Following:(id)sender;

@end
