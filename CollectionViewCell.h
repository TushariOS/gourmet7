//
//  CollectionViewCell.h
//  Gourmet7
//
//  Created by Mac Pro on 09/12/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *restimages;

@end
