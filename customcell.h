
#import <UIKit/UIKit.h>
#import "RateView.h"

@interface customcell : UITableViewCell <RateViewDelegate>
{
   bool follow;
}
@property (weak, nonatomic) IBOutlet UIButton *followbtn;
@property (weak, nonatomic) IBOutlet UILabel *restonamelbl;
@property (weak, nonatomic) IBOutlet UILabel *Arealbl;
@property (weak, nonatomic) IBOutlet UIImageView *doorimg;
@property (weak, nonatomic) IBOutlet UILabel *discountlbl;
@property (weak, nonatomic) IBOutlet RateView *rateview;

@property (weak, nonatomic) IBOutlet UIButton *followeresto;
@property (weak, nonatomic) IBOutlet UIImageView *compliimage;

@property (weak, nonatomic) IBOutlet UIButton *callbutton;

@property (weak, nonatomic) IBOutlet UILabel *Speciality;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *Distancelbl;
@property (weak, nonatomic) IBOutlet UILabel *Namelabe;

@end
