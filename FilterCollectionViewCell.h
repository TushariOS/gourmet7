//
//  FilterCollectionViewCell.h
//  Gourmet7
//
//  Created by Mac Pro on 29/01/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterCollectionViewCell : UICollectionViewCell
@property (strong,nonatomic)IBOutlet UIImageView *Facilityimages;
@property (strong, nonatomic)IBOutlet UILabel *Facilityname;

@end
