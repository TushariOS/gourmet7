//
//  AboutViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 12/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface AboutViewController : UIViewController<SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *aboutimageview;
@property (weak, nonatomic) IBOutlet UIView *buttonsview;
@property (strong, nonatomic)UITextView *myTextView;
@property (strong, nonatomic)NSString *termsofservice;
@property (strong, nonatomic)NSString *Privacypolicy;
- (IBAction)termsofuse:(id)sender;
- (IBAction)Privacypolicy:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarbtn;

@end
