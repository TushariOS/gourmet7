//
//  PicGallery.h
//  Gourmet7
//
//  Created by Mac Pro on 25/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "CollectionViewCell.h"

@interface PicGallery : CollectionViewCell
@property (strong, nonatomic)NSArray *PicArray;
@property (weak, nonatomic) IBOutlet UIImageView *FullImage;
@property (weak, nonatomic) IBOutlet UIButton *Closebtn;

@end
