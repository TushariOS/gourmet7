//
//  MySpaceTableViewCell.h
//  Gourmet7
//
//  Created by Mac Pro on 07/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MySpaceTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *Datelabel;
@property (weak, nonatomic) IBOutlet UILabel *timelabel;
@property (weak, nonatomic) IBOutlet UILabel *purposelabel;
@property (weak, nonatomic) IBOutlet UILabel *savedlabel;

@end
