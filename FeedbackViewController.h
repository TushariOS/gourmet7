//
//  FeedbackViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 23/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *FeedbackTable;

@property (weak, nonatomic) IBOutlet UITextView *Enterfeedback;

- (IBAction)Submitbtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *feedbacktxtview;
@property (strong, nonatomic) NSString *outletId;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *token;
@property (strong, nonatomic)NSString *Firstname;
@property (strong, nonatomic)NSString *lastName;
@property (strong, nonatomic) NSString *date;
@property (strong, nonatomic)NSString *feedback;
@property (strong, nonatomic)NSString *fullname;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic)NSString *Fdate;
@property (strong, nonatomic)NSString *finalTime;
-(IBAction)BAck_btn:(id)sender;

@end
