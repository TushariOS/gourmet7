//
//  PremiumCollectionViewCell.m
//  Gourmet7
//
//  Created by Mac Pro on 24/06/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "PremiumCollectionViewCell.h"

@implementation PremiumCollectionViewCell
@synthesize Facilityname;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    Facilityname.lineBreakMode = NSLineBreakByWordWrapping;
    Facilityname.numberOfLines = 0;
}
@end
