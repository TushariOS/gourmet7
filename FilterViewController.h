//
//  FilterViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 23/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"



@class FilterViewController;

@protocol FilterViewControllerDelegate <NSObject>

- (void)PassId:(FilterViewController *)FilterViewController PassedId:(NSMutableArray *)ID;
- (void)PassvisitPurpose:(FilterViewController *)FilterViewController PassvisitPurpose:(NSMutableArray *)MyArray;
@end

@interface FilterViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate,SWRevealViewControllerDelegate>
{
    bool open,compli,discount,following;
}
@property (nonatomic, weak) id <FilterViewControllerDelegate> delegate;
@property (strong, nonatomic) NSMutableArray *FilterArray;


@property (strong, nonatomic) NSMutableArray *PremiumArray;


@property (strong, nonatomic) NSMutableArray *FacilityArray;
@property (strong, nonatomic) NSMutableArray *FinalArray;
@property (strong, nonatomic)NSMutableArray *Sectionheader;

@property (weak, nonatomic) IBOutlet UITextField *RestoNameText;
@property (weak, nonatomic) IBOutlet UITextField *CuisineText;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarbtn;
- (IBAction)Backbutton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *AreaText;

- (IBAction)Searchbtn:(id)sender;



@end
