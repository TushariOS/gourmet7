//
//  HelpViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 12/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface HelpViewController : UIViewController<SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarbtn;

@end
