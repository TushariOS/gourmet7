//
//  DropdownlistViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 28/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SUser.h"

@interface DropdownlistViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *droplisttable;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;

@property (strong, nonatomic)NSString *Currentcity;
@property (strong, nonatomic)NSArray *MyArray;
@property (strong, nonatomic)NSString *CityName;
@property (strong, nonatomic)SUser *Data;
@property (strong, nonatomic)SUser *aUser;
@property (strong, nonatomic)NSMutableString *SearchStrUser;
@property (strong, nonatomic)NSMutableArray *SearchResultCity;
@property (strong, nonatomic)NSMutableArray *SearchResultCityId;
@property (strong, nonatomic)NSDictionary *Citydictionary;

@property BOOL isFiltered;
@end
