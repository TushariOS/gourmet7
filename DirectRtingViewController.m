//
//  DirectRtingViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 22/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "DirectRtingViewController.h"
#import "SUserDB.h"


@interface DirectRtingViewController ()

@end

@implementation DirectRtingViewController
@synthesize Foodratingview,Ambienceratingview,Serviceratingview,Valueformoneyview,HygieneRatingview,rates1,rates2,rates3,rates4,rates5,token,OutletId,userId,data2;

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"%@",OutletId);
    token = [[SUserDB singleton]fetchTokenNumber];
    userId = [[SUserDB singleton]fetchuserId];
    
    [self youratedUrl];
    
    self.Foodratingview.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Foodratingview.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Foodratingview.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Foodratingview.rating = data2.FRating;
    self.Foodratingview.editable = YES;
    self.Foodratingview.maxRating = 5;
    self.Foodratingview.delegate = self;
    
    self.Ambienceratingview.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Ambienceratingview.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Ambienceratingview.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Ambienceratingview.rating1 = data2.Arating;
    self.Ambienceratingview.editable = YES;
    self.Ambienceratingview.maxRating = 5;
    self.Ambienceratingview.delegate = self;
    
    self.Serviceratingview.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Serviceratingview.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Serviceratingview.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Serviceratingview.rating2 = data2.Srating;
    self.Serviceratingview.editable = YES;
    self.Serviceratingview.maxRating = 5;
    self.Serviceratingview.delegate = self;
    
    self.Valueformoneyview.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.Valueformoneyview.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.Valueformoneyview.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.Valueformoneyview.rating3 = data2.Vrating;
    self.Valueformoneyview.editable = YES;
    self.Valueformoneyview.maxRating = 5;
    self.Valueformoneyview.delegate = self;
    
    self.HygieneRatingview.notSelectedImage = [UIImage imageNamed:@"Ratings-Click1.png"];
    self.HygieneRatingview.halfSelectedImage = [UIImage imageNamed:@"Ratings-Click-Half.png"];
    self.HygieneRatingview.fullSelectedImage = [UIImage imageNamed:@"Ratings-Click-full.png"];
    self.HygieneRatingview.rating4 = data2.Hrating;
    self.HygieneRatingview.editable = YES;
    self.HygieneRatingview.maxRating = 5;
    self.HygieneRatingview.delegate = self;
    
    
}

-(void)viewDidUnload
{
    [self setFoodratingview:nil];
    [self setAmbienceratingview:nil];
    [self setServiceratingview:nil];
    [self setValueformoneyview:nil];
    [self setHygieneRatingview:nil];
}



-(void)RatingUrl
{
    
//    NSString *string = [NSString stringWithFormat:@"http://www.gourmet7.com/auth?url=/Restaurant/saveRatingValues&userId=%@&outletId=%@&rating1=%f&rating2=%f&rating3=%f&rating4=%f&rating5=%f&token=%@",userId,OutletId, rates1,rates2,rates3,rates4,rates5,token];
//    
//    NSURL *url = [NSURL URLWithString:string];
    
    
    
    NSString *Rateurl = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=/Restaurant/saveRatingValues&userId=%@&outletId=%@&rating1=%f&rating2=%f&rating3=%f&rating4=%f&rating5=%f&token=%@",userId,OutletId, rates1,rates2,rates3,rates4,rates5,token];
    [self callService:Rateurl];
    NSLog(@"rating submited");
}


-(void)callService :(NSString *)str
{
    NSURL *urlStr=[NSURL URLWithString:str];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlStr];
    [request setTimeoutInterval:60.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue ]completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         NSLog(@"%@",response);
         if(response!=nil)
         {
             NSError *parseJsonError;
             NSMutableDictionary *serviceResponse=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parseJsonError];
             [self Parse:serviceResponse];
             
             NSLog(@"error is : %@",parseJsonError);
         }
         
     }];
}
-(void)Parse:(NSMutableDictionary *)response
{
    NSLog(@"response is %@",response);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)ratesView:(RatesView *)ratesView ratingDidChange:(float)rating
{
    rates1 = rating;
    
    NSLog(@"rating is :%f",rating);
}


-(void)Ambiencerating:(Ambiencerating *)Ambiencerating ratingDidChange:(float)rating1
{
    rates2 = rating1;
    
    NSLog(@"rating1 is :%f",rating1);
}
-(void)Servicerating:(Servicerating *)Servicerating ratingDidChange:(float)rating2
{
    rates3 = rating2;
    
    NSLog(@"rating2 is :%f",rating2);
     
    
}
-(void)ValueforMoney:(ValueforMoney *)ValueforMoney ratingDidChange:(float)rating3
{
    rates4 = rating3;
    NSLog(@"rating3 is :%f",rating3);
}
-(void)Hygienerating:(Hygienerating *)Hygienerating ratingDidChange:(float)rating4
{
    rates5 = rating4;
    NSLog(@"rating4 is :%f",rating4);
}

-(void)youratedUrl
{
    NSString *url = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth?url=/Restaurant/RatedRestaurantValue/%@/%@&token=%@",userId, OutletId, token];
    [self callService1:url];
}

-(void)callService1 :(NSString *)str
{
    NSURL *urlStr=[NSURL URLWithString:str];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:urlStr];
    [request setTimeoutInterval:60.0];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    NSError *requesterror = nil;
    NSURLResponse *urlResponse1 = nil;
    NSData *response2 = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse1 error:&requesterror];
    
    NSLog(@"%@",response2);
    if (response2!=nil)
    {
        NSError *parseJsonError;
        NSMutableDictionary* serviceResponse=[NSJSONSerialization JSONObjectWithData:response2 options:NSJSONReadingMutableContainers error:&parseJsonError];
        
        [self Parse1:serviceResponse];
    }
}
-(void)Parse1:(NSMutableDictionary *)response
{
    data2 = [SUser2 new];
    
    data2.FRating = [[response valueForKey:@"rating1"]floatValue];
    data2.Arating = [[response valueForKey:@"rating2"]floatValue];
    data2.Srating = [[response valueForKey:@"rating3"]floatValue];
    data2.Vrating = [[response valueForKey:@"rating4"]floatValue];
    data2.Hrating = [[response valueForKey:@"rating5"]floatValue];
}
- (IBAction)Ratebutton:(id)sender
{
    [self RatingUrl];
 [self.navigationController popViewControllerAnimated:YES];
    //[self dismissViewControllerAnimated:YES completion:nil];

    
}
- (IBAction)BackBtnClicked:(id)sender {
   [self.navigationController popViewControllerAnimated:YES];
}
@end
