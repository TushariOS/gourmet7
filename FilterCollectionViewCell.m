//
//  FilterCollectionViewCell.m
//  Gourmet7
//
//  Created by Mac Pro on 29/01/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "FilterCollectionViewCell.h"
#import "RestaurantListing.h"

@implementation FilterCollectionViewCell
@synthesize Facilityname;
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    Facilityname.lineBreakMode = NSLineBreakByWordWrapping;
    Facilityname.numberOfLines = 0;
}
@end

