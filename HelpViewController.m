//
//  HelpViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 12/04/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()

@end

@implementation HelpViewController
@synthesize sidebarbtn;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.revealViewController.delegate = self;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarbtn setTarget: self.revealViewController];
        [self.sidebarbtn setAction: @selector( mysidebarAnimated:)];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
