//
//  ShowPicViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 29/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "ShowPicViewController.h"
#import "PicGallery.h"
#import "UIImageView+WebCache.h"

@interface ShowPicViewController ()

@end

@implementation ShowPicViewController
@synthesize MyArray;

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"the array is :",MyArray);
    // Do any additional setup after loading the view.
}
//
//NSURL *url = [mobilethumbimageArray objectAtIndex:indexPath.row];
//[cell.restimages sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"default_144x144.png"]];
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView
    numberOfItemsInSection:(NSInteger)section
{
    return MyArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"cell";
    
    PicGallery *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    NSURL *url = [MyArray objectAtIndex:indexPath.row];
    [cell.FullImage sd_setImageWithURL:url placeholderImage:[UIImage imageNamed:@"default_144x144.png"]];
    
    cell.selected = YES;
    [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionLeft];
    [cell.layer setBorderWidth:2.0f];
    [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
    [cell.Closebtn addTarget:self
                       action:@selector(aMethod:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}
-(void)aMethod:(UIButton*)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
