//
//  NSString+URLEncoding.h
//  Gourmet7
//
//  Created by Astrika Infotech on 13/10/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSString (URLEncoding)
-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;
@end
@interface NSString_URLEncoding : NSString

@end