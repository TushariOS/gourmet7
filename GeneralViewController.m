//
//  GeneralViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 02/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import "GeneralViewController.h"
#import "GenOfferDetailsViewController.h"

@interface GeneralViewController ()
@end
@implementation GeneralViewController
@synthesize Mybutton,MyDict,myArray,Dict,offer,offertitle,groupdetails,TandC;

- (void)viewDidLoad
{
    [super viewDidLoad];
    }

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    }

- (void)reloadGeneral:(NSNotification *)anote
{
    MyDict = [anote userInfo];
    
    myArray = [MyDict objectForKey:@"generalOffers"];
    if (!myArray || !myArray.count)
    {
        self.Mybutton.hidden = YES;
    
    }
    else
    {
        Dict = [myArray objectAtIndex:0];
        offer = [Dict objectForKey:@"description"];
        offertitle = [Dict objectForKey:@"title"];
        groupdetails = [Dict objectForKey:@"groupDetails"];
        TandC = [Dict objectForKey:@"termsandcondition"];
    }
    [self.Mybutton setTitle:[NSString stringWithFormat:@"%@",offertitle] forState:UIControlStateNormal];
}


- (IBAction)button:(id)sender
{
    GenOfferDetailsViewController *genVc = [self.storyboard instantiateViewControllerWithIdentifier:@"Genoffer"];
  [self.navigationController pushViewController:genVc animated:YES];
    NSLog(@"%@",myArray);
    genVc.offerArray = myArray;
    NSLog(@"%@",genVc.offerArray);
}

-(void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadGeneral:) name:@"general" object:nil];
}
@end
