//
//  GenOfferDetailsViewController.h
//  Gourmet7
//
//  Created by Mac Pro on 02/03/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GenOfferDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *offerlbl;
@property (weak, nonatomic) IBOutlet UIButton *closebtn;
@property (weak, nonatomic) IBOutlet UITextView *offertxtview;
@property (weak, nonatomic) IBOutlet UIView *Myview;
@property (strong, nonatomic)NSString *offer;
@property (strong, nonatomic)NSString *offerdesc;
@property (strong, nonatomic)NSArray *offerArray;
@property (weak, nonatomic) IBOutlet UILabel *grouplbl;
@property (weak, nonatomic) IBOutlet UILabel *conditionlbl;
@property (strong,nonatomic)NSDictionary *offerDict;
@property (strong, nonatomic)NSArray *groupdetails;
@property (strong, nonatomic) NSString *terms;


- (IBAction)Groupsbtn:(id)sender;
- (IBAction)Tandconds:(id)sender;

@end
