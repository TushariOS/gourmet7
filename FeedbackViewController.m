//
//  FeedbackViewController.m
//  Gourmet7
//
//  Created by Mac Pro on 23/11/15.
//  Copyright (c) 2015 Sagar Pachorkar. All rights reserved.
//

#import "FeedbackViewController.h"
#import "SUserDB.h"
#import "FeedbackTableViewCell.h"
#import "AFNetworking.h"

@interface FeedbackViewController ()


@end

@implementation FeedbackViewController
{
    NSMutableArray *FeedArray;
}
@synthesize feedbacktxtview,Enterfeedback,outletId,userId,token,date,feedback,Firstname,lastName,fullname,time,Fdate,finalTime;

- (void)viewDidLoad
{
    token = [[SUserDB singleton]fetchTokenNumber];
    userId = [[SUserDB singleton]fetchuserId];
    Firstname = [[SUserDB singleton]fetchfirstname];
    lastName = [[SUserDB singleton]fetchlastname];
    fullname = [NSString stringWithFormat:@"%@ %@",Firstname,lastName];
    [super viewDidLoad];
    UIImage *logoimage = [UIImage imageNamed:@"logo_small.png"];
    
    self.navigationItem.titleView = [[UIImageView alloc]initWithImage:logoimage];
    

    
    [[self FeedbackTable]setDataSource:self];
    [[self FeedbackTable]setDelegate:self];
    [self Feedbackurl];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(IBAction)BAck_btn:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)Feedbackurl
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.securityPolicy.allowInvalidCertificates = YES;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",nil];
    
    NSString *URLString = [NSString stringWithFormat:@"http://167.114.0.116:9090/auth"];
    NSDictionary *params = @{@"feedbackMsg": self.Enterfeedback.text,
                             @"outletId": outletId,
                             @"memberId":userId,
                             @"url":@"/Restaurant/SubmitFeedback",
                             @"token":token};
    
    
    [manager POST:URLString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSLog(@"JSON: %@", responseObject);
         
         [self catchfeedback:responseObject];
         
              } failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         NSLog(@"Error: %@", error);
     }];
}

-(void)catchfeedback:(NSDictionary *)JsonData
{
    date = [JsonData valueForKey:@"createdDate"];
    feedback = [JsonData valueForKey:@"feedback"];
    
    NSString* timeStampString =date;
    NSTimeInterval interval=[timeStampString doubleValue];
    NSTimeInterval times = interval/1000;
    NSDate *date12 = [NSDate dateWithTimeIntervalSince1970:times];
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *_date=[_formatter stringFromDate:date12];
    
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc]init];
    [formatter1 setDateFormat:@"HH:mm:ss"];
    
    NSString *date1 = [formatter1 stringFromDate:date12];
    Fdate = _date;
    time = date1;
    finalTime = [NSString stringWithFormat:@"%@ %@",_date,date1];
    
    [[self FeedbackTable ]reloadData];
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return 2;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    static NSString *cellidentifier = @"feebackcell";
    FeedbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellidentifier forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[FeedbackTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellidentifier];
        
    }
    cell.name.text = fullname;
    cell.dateandtime.text = finalTime;
    cell.feedback.text = feedback;
    return cell;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}
- (IBAction)Submitbtn:(id)sender
{
    [self Feedbackurl];
    
    }


@end
