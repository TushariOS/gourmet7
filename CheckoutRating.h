//
//  CheckoutRating.h
//  Gourmet7
//
//  Created by Mac Pro on 23/02/16.
//  Copyright © 2016 Sandip Khedkar. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatesView.h"
#import "Ambiencerating.h"
#import "Servicerating.h"
#import "ValueforMoney.h"
#import "Hygienerating.h"
#import "YouRatedView.h"
#import "CheckinViewController.h"
#import "SUserDB.h"
#import "SUser2.h"
#import "SUser.h"

@interface CheckoutRating : UIViewController<RatesViewDelegate,AmbienceratingDelegate,ServiceratingDelegate,ValueforMoneyDelegate,HygieneratingDelegate,YouRatedViewDelegate>

@property (weak, nonatomic) IBOutlet RatesView *Foodrating;
@property (weak, nonatomic) IBOutlet Ambiencerating *Ambience;
@property (weak, nonatomic) IBOutlet Servicerating *Servicerating;
@property (weak, nonatomic) IBOutlet ValueforMoney *valueformonyrting;
@property (weak, nonatomic) IBOutlet Hygienerating *Hygienerting;
@property (weak, nonatomic) IBOutlet YouRatedView *YouRated;

@property (nonatomic) float rates1;
@property (nonatomic) float rates2;
@property (nonatomic) float rates3;
@property (nonatomic) float rates4;
@property (nonatomic) float rates5;
@property (strong,nonatomic) NSString *token;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *requestId;
@property (strong, nonatomic) NSString *visitpurpose;
@property (strong, nonatomic) NSDictionary *RateDict;
@property (strong, nonatomic)  NSString *outletId1;
@property (strong, nonatomic) NSString *msg;
@property (strong, nonatomic)NSString *message;
@property (strong, nonatomic) NSString *Vpurpose;

@property (strong, nonatomic) SUser2 *data2;
@property (weak, nonatomic) IBOutlet UITextView *messagetext;

@property (strong, nonatomic) UIBarButtonItem *submit;
- (IBAction)Ratebutton:(id)sender;
- (IBAction)Cancelbutton:(id)sender;
@end
